import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
// import { LPAuthGuard } from 'laravel-passport';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule'},
  { path: 'tab-listainvitaciones', loadChildren: './tab-listainvitaciones/tab-listainvitaciones.module#TabListainvitacionesPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'qrscanner', loadChildren: './qrscanner/qrscanner.module#QrscannerPageModule' },
  // { path: 'qrcode', loadChildren: './qrcode/qrcode.module#QrcodePageModule' },
  { path: 'forgot', loadChildren: './forgot/forgot.module#ForgotPageModule' },
  { path: 'qrcode', loadChildren: './qrcode/qrcode.module#QrcodePageModule' },

  { path: 'categorizedlocals', loadChildren: './categorizedlocals/categorizedlocals.module#CategorizedlocalsPageModule' },
  { path: 'reservation', loadChildren: './reservation/reservation.module#ReservationPageModule' },
  { path: 'reviewdetails', loadChildren: './reviewdetails/reviewdetails.module#ReviewdetailsPageModule' },
  { path: 'pruebas', loadChildren: './pruebas/pruebas.module#PruebasPageModule' },
  { path: 'completargoog', loadChildren: './completargoog/completargoog.module#CompletargoogPageModule' },
  { path: 'algo', loadChildren: './tabs/algo/algo.module#AlgoPageModule' },
  { path: 'tab-calendar', loadChildren: './tab-calendar/tab-calendar.module#TabCalendarPageModule' },
  { path: 'tab-checatuseventos', loadChildren: './tab-checatuseventos/tab-checatuseventos.module#TabChecatuseventosPageModule' },
  { path: 'imagencompleteregistro', loadChildren: './imagencompleteregistro/imagencompleteregistro.module#ImagencompleteregistroPageModule' },

  { path: 'confirmesucorreo', loadChildren: './confirmesucorreo/confirmesucorreo.module#ConfirmesucorreoPageModule' },
  { path: 'ocupacionescompleteregistro', loadChildren: './ocupacionescompleteregistro/ocupacionescompleteregistro.module#OcupacionescompleteregistroPageModule' },
  { path: 'ocu2completeregistro', loadChildren: './ocu2completeregistro/ocu2completeregistro.module#Ocu2completeregistroPageModule' },  { path: 'mediciones', loadChildren: './mediciones/mediciones.module#MedicionesPageModule' },
  { path: 'medicionespreload', loadChildren: './medicionespreload/medicionespreload.module#MedicionespreloadPageModule' },
  { path: 'habilidades', loadChildren: './habilidades/habilidades.module#HabilidadesPageModule' },
  { path: 'habilidadespreload', loadChildren: './habilidadespreload/habilidadespreload.module#HabilidadespreloadPageModule' },
  { path: 'caracteristicas', loadChildren: './caracteristicas/caracteristicas.module#CaracteristicasPageModule' },
  { path: 'caracteristicaspreload', loadChildren: './caracteristicaspreload/caracteristicaspreload.module#CaracteristicaspreloadPageModule' },
  { path: 'tareas', loadChildren: './tareas/tareas.module#TareasPageModule' },
  { path: 'tareaspreload', loadChildren: './tareaspreload/tareaspreload.module#TareaspreloadPageModule' },
  { path: 'imagenespreload', loadChildren: './imagenespreload/imagenespreload.module#ImagenespreloadPageModule' },
  { path: 'imagenes', loadChildren: './imagenes/imagenes.module#ImagenesPageModule' },











//  { path: 'eventcreate', loadChildren: './eventcreate/eventcreate.module#EventcreatePageModule' },

  // { path: 'qr-promotion', loadChildren: './qr-promotion/qr-promotion.module#QrPromotionPageModule' },
  // { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
