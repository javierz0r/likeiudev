import { Component } from '@angular/core';
import { JsonService } from './json.service';
import { HttpClient } from '@angular/common/http';
import { TabUserPage } from './tab-user/tab-user.page';
import { LoginPage } from './login/login.page';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { timer } from 'rxjs';


import { Platform,MenuController,NavController} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  showSplash = true; // <-- show animation
  rootPage:any = './login/login.page';
  navigate : any;
  navigatecustombyjavier : any;
  user: any;
  private isUserLoggedIn;
  public usserLogged:TabUserPage;
  
  nav: NavController;
  constructor(
    public router: Router,
    public storage: NativeStorage,
    public menu: MenuController,
    private platform: Platform,
    private splashScreen: SplashScreen,
    public json: JsonService,
    private http: HttpClient,
    private statusBar: StatusBar,
    private googlePlus: GooglePlus
  ) 
  
  {
    // this.LocalStorageSessionPost();
    this.LocalStorageSessionGet();
    this.initializeApp();
    this.sideMenu();
 
  }

LocalStorageSessionGet()
{
  console.log(localStorage);
  return JSON.parse(localStorage.getItem('access_token'));
}

  initializeApp() {


    this.platform.ready().then(() => {


      this.statusBar.styleDefault();
        //animation splash:
      this.splashScreen.hide();  // <-- hide static image
      timer(5000).subscribe(() => this.showSplash = false) // <-- hide animation after 3s
  //finish animation 
      //this.sideMenu();
    });
  }

  

  // backToWelcome(){
  //   //const root = this.app.getRootNav();
  //   //root.popToRoot();
  //   this.nav.setDirection(this.rootPage);
  // }

  logoutClicked(){
    this.googlePlus.logout();
    // this.menu.enable(false);
    localStorage.clear();
    //this.backToWelcome();
    console.log(localStorage);
   // this.menu.close();
    this.router.navigate(['/login']);
    this.googlePlus.logout();
  }


  qrcode(){
    this.router.navigate(['/tabs/qrcode']);
        this.menu.close();
        //  this.menu.enable(false);

  }
  


  sideMenu()
  {
    this.navigate =
    [
      {
        title : "MI PERFIL",
        url   : "/tabs/tab-user",
        icon  : "person"
      }
,
      {
        title : "QR GENERATE",
        url   : "/tabs/qrcode",
        icon  : "qr-scanner"  
      },
 

      
    ]



  }


//:::::::PARTE DEL CODIGO INSERVIBLE:::::::

  pages = [
    {

      title: 'MIS MENSAJES',
      url: '/menu/main',
      icon: 'home',
      children: [
        {
          title: 'Notificaciones',
          url: '/menu/ionic',
          icon: 'logo-ionic'
        },



        {
          title: 'Mis Mensajes',
          url: '/menu/flutter',
          icon: 'logo-google'
        },

        
      ]
    }

    
  ];


  pagescustom = [
    {

      icon: 'home',
      children: [
        {
          icon: 'logo-ionic'
        },

      ]
    }

    
  ];




  openpageqrcode() {


    this.router.navigate(['/tabs/qrcode']);

    
  }






//:::::::FIN DE LA PARTE DEL CODIGO INSERBIBLE:::::::


  
  ionViewWillEnter() {
    this.storage.getItem('first').then(res => {
      if (res === true) {
        this.router.navigateByUrl('/app/tabs/home');
      }
    });

    this.menu.enable(false);
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}
