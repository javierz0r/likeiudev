import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { AppRoutingModule } from './app-routing.module';
import { IonicRatingModule } from 'ionic4-rating';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { ImagePicker } from '@ionic-native/image-picker/ngx';

import { RouteReuseStrategy } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { JsonService } from './json.service';

import { TabUserPage } from './tab-user/tab-user.page';

import { AppComponent } from './app.component';
import { LaravelPassportModule } from 'laravel-passport';
// import { GoogleMaps } from '@ionic-native/google-maps';
import { HttpClientModule } from '@angular/common/http';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@ionic-native/file/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { SharedModule } from './app.shared.module';
import {ProgressBarModule} from "angular-progress-bar"



import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { environment } from '../environments/environment';


import { HttpModule } from '@angular/http';
import { PipesModule } from './pipes/pipes.module';
@NgModule({
  declarations: [
    AppComponent,
  ],
  entryComponents: [],
  imports: [
  SharedModule,
  PipesModule,
  HttpClientModule,
  // ImagePicker,
  NgxQRCodeModule,
  BrowserModule,
  BrowserAnimationsModule,
  IonicRatingModule,
  IonicModule.forRoot(),
  AppRoutingModule,
  ProgressBarModule,
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireAuthModule,
  HttpModule,
   LaravelPassportModule.forRoot({apiRoot:'http://api.likeiu.com',
   clientId: 4,
   clientSecret: 'kZ4xUzC0jf3mhZrJ9JgHQwEdJDSM0FGj1CnrlMX2'
       })],
  providers: [
    StatusBar,
    TabUserPage,
    JsonService,
    SplashScreen,
    GooglePlus,
    // GoogleMaps,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    SocialSharing,
    File,
    Geolocation,
    NativeStorage,
    NativeGeocoder
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
