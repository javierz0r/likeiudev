import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CaracteristicaspreloadPage } from './caracteristicaspreload.page';

const routes: Routes = [
  {
    path: '',
    component: CaracteristicaspreloadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CaracteristicaspreloadPage]
})
export class CaracteristicaspreloadPageModule {}
