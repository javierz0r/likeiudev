import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorizedlocalsPage } from './categorizedlocals.page';

describe('CategorizedlocalsPage', () => {
  let component: CategorizedlocalsPage;
  let fixture: ComponentFixture<CategorizedlocalsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategorizedlocalsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorizedlocalsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
