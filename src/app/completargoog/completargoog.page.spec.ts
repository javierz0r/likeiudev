import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletargoogPage } from './completargoog.page';

describe('CompletargoogPage', () => {
  let component: CompletargoogPage;
  let fixture: ComponentFixture<CompletargoogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletargoogPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletargoogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
