import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { JsonService } from '../../json.service';

@Component({
  selector: 'app-animated-like',
  templateUrl: './animated-like.component.html',
  styleUrls: ['./animated-like.component.scss'],
  animations: [
    trigger('heart', [
        state('unliked', style({
            color: '#fff',
            opacity: '0.5',
            transform: 'scale(1)'
        })),
        state('liked', style({
            color: '#e74c3c',
            opacity: '1',
            transform: 'scale(1.1)'
        })),
        transition('unliked <=> liked', animate('100ms ease-out'))
    ])
  ]
})
export class AnimatedLikeComponent implements OnInit {
  public likeState: string = 'unliked';
  public iconName: string = 'heart-empty';
  public inconValue: number;
  clickedpromotion: any;
  point_granted: any;
  total_basic_points: any;
  total_gold_points: any;
  activatedRoute: any;
  like_promotion: any;

  constructor(
    private json: JsonService,
  ) { localStorage.getItem('currentUser');}

  ngOnInit()
   {

    // var localemail = localStorage['currentUser'];
    // var email = {email: localemail};
 
    // this.json.PostFavoritelike(email).subscribe((res: any ) =>{
    //  this.like_promotion = res.like_promotion;
    //   console.log(res)
    // });
   }
  id_function(id_function: any) {
    throw new Error("Method not implemented.");
  }

  toggleLikeState(){




    if(this.likeState == 'unliked')
    {
      this.likeState = 'liked';
      this.iconName = 'heart';
      this.inconValue = 1;
    } else {
      this.likeState = 'unliked';
      this.iconName = 'heart-empty';
      this.inconValue = 0;
    }

  }

}
