import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfirmesucorreoPage } from './confirmesucorreo.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmesucorreoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConfirmesucorreoPage]
})
export class ConfirmesucorreoPageModule {}
