import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { JsonService } from '../json.service';
import { LoadingController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-confirmesucorreo',
  templateUrl: './confirmesucorreo.page.html',
  styleUrls: ['./confirmesucorreo.page.scss'],
})
export class ConfirmesucorreoPage implements OnInit {
  user_id: any;
  local_id: any;
  codigoingresado: any;
  status: any;
  reservate_date: any;
  fuctionconsult: any;
  data: any;
  sigin: any;
  email: any;
  name: any;
  last_name: any;
  phone: any;
  password: any;
  password_confirmation: any;
  hideMe: any;
  // data: any;
  insertaeste: any;
  request: string;
  constructor( 
    public menu: MenuController,    
    private json: JsonService,
    private activatedRoute: ActivatedRoute, 
    public loadingController: LoadingController,
    private router: Router) 
  
  
  { 

    this.activatedRoute.queryParams
    .subscribe((res: any)=>{
    this.data = res;//subscribing response
    console.log('recibi esto de la pagina anterior por parametros de navegacion', this.data);
  });
  


//EL SIGUIENTE CONTROLADOR TRAE UN CODIGO ALEATORIO DE LA TABLA verificationcode  DE LIKEIU.
this.json.Codeshow().subscribe((res: any) =>{
  this.codes = res;
  console.log('RESPUESTA Y OBJECTO TRAIDO', this.codes);
  console.log('ESTE SERA EL CODIGO A CONFIRMAR:', this.codes[0].code);
  this.insertaeste= this.codes[0].code;
  this.activatedRoute.queryParams
  .subscribe((res: any)=>{
  this.data = res;//subscribing response
  console.log('recibi esto de la pagina anterior por parametros de navegacion', this.data);



  var code = {
    email: this.data.email,
    vericode: this.codes[0].code,
    appverification: '1',
    };
  console.log('enviando este codigo datos a LikeIU users:', code);
  localStorage.setItem('currentUser', JSON.stringify(code.email));
  this.json.thiscodetothisuser(code).subscribe((res: any ) =>{
    console.log('respuesta del inser',res);
    //se enviara el codigo por correo electronico:
      var localemail = localStorage.currentUser;
      var correodata = {
        amount_person: this.codes[0].code,
        emailcorreo: localemail
      };
      localStorage.setItem('compararcodigo', this.codes[0].code);

      this.json.emailConfirmation(correodata).subscribe((res: any ) =>{
    });
  });


});
});




  
  }
  

  ngOnInit() {



  }


  codes(codes: any) {
    throw new Error("Method not implemented.");
  }

  hide() {

    if(this.hideMe==true){

    this.hideMe = false;
  }

    else{

      this.hideMe = true;
    };
  }
  

  backloginpage(){
    this.router.navigate(['/login'],);
  }

async  confirmarcodigo(){

if(this.codigoingresado==localStorage.compararcodigo){

//confirmo correo: nivel de registro Nro:2:
var code = {
  email: this.data.email,
  vericode: this.codes[0].code,
  appverification: '2',
  };

  
this.json.thiscodetothisuser(code).subscribe((res: any ) =>{
  console.log('respuesta del inser',res);


});



console.log('coloco el codigo bien');

//DESACTIVADO HASTA QUE SE CREE LA GALERIA DE SUBIR FOTOS
// this.router.navigate(['/imagencompleteregistro'],);
//DESACTIVADO POR QUE PAUSARON LA SUBIDA DE IMAGENES AL HOSTING POR TIEMPO
this.router.navigate(['/ocupacionescompleteregistro'],);
//Mantendremos un registro del correo verificado...
localStorage.setItem('currentUser', JSON.stringify(this.data.email));
console.log('se ha verificado exitosamente este correo:',this.data.email)

}
else{
    const loadingerror = await this.loadingController.create({
      message: 'ERROR: Codigo Ingresado incorrecto...',
      spinner: 'dots',
      duration: 1500,
    });
    return  loadingerror.present();
console.log('codigo incorrecto');

}



  }


  ionViewWillEnter() {
    this.menu.enable(false);
  }
  
  
  ionViewDidLeave() {
    // enable the root left menu because is login page.
    this.menu.enable(true);
  }



}
