import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarnPointsPage } from './earn-points.page';

describe('EarnPointsPage', () => {
  let component: EarnPointsPage;
  let fixture: ComponentFixture<EarnPointsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarnPointsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarnPointsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
