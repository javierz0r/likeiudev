import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';
import { map } from "rxjs/operators";
import { Router } from "@angular/router";

@Component({
  selector: 'app-earn-points',
  templateUrl: './earn-points.page.html',
  styleUrls: ['./earn-points.page.scss'],
})
export class EarnPointsPage implements OnInit {
  estadisticasshow: any;

  constructor
  
  (
    private json: JsonService,
    private router: Router

  ) 

  { 
    localStorage.getItem('currentUser');




  }
  ngOnInit() {

    //se procedera a filtrar las mediciones:::..
    var datafiltado = {
      email: localStorage.currentUser,
    }
    
    console.log('Este usuario desea conocer sus estadisticas correspondisntes segun su User ID y la tabla estadisticas_user:', datafiltado)
    // this.json.filtrodemediciones(datafiltado).subscribe((res: any ) =>{
    //     console.log('respuesta del filtro', res);
    // });
    //filtradas.
        return new Promise(resolve => {
    
        this.json.filtrodeestadisticas(datafiltado)
        .pipe(map(res => res))
        .subscribe((res:any)  =>{

            this.estadisticasshow = res;
            resolve(this.estadisticasshow);
            console.log(this.estadisticasshow)
        });
      
    });
    
    
      }


}
