import { Component, OnInit } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { JsonService } from '../json.service';
import { NavController,LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  hideMe: boolean;
  tengolaimagen: any;
  imageResponse: any;
  options: any;
  
  constructor
  (
    public loadingController: LoadingController,
    private json: JsonService,
    private imagePicker: ImagePicker    
  )
  { }

  ngOnInit() {
    this.hideMe = true;
  }

async getImages() {

    this.hideMe = false;


    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      maximumImagesCount: 1,
  
      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      height: 200,
      //height: 200,
  
      // quality of resized image, defaults to 100
      quality: 25,
  
      // output type, defaults to FILE_URIs.
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then(async (results) => {
      //se preparo imageResponse como un arreglo
      //se pueden almacenar varias segun la configuracion de arriba, 
      //pero como solo esta para una foto, el array 0 la contiene
      for (var i = 0; i < results.length; i++) {

        // this.imageResponse.push('data:image/jpeg;base64,' + results[i]);

        this.imageResponse.push(results[i]);
        // this.imageResponse['0']='asd';
        this.tengolaimagen=this.imageResponse['0'];
        console.log(this.imageResponse['0']);
        var localemail = localStorage['currentUser'];
        var data = {
          email: localemail,
          photo64: this.imageResponse['0']
          };

          const loading = await this.loadingController.create({
            message: 'Cambiando Imagen',
          });

        console.log('Datos enviados para update user:', data);
        
        this.json.UpdateImg(data).subscribe(async (res: any ) =>{
          await loading.present();
          console.log('soy la respuesta',res);
          await loading.onDidDismiss();
          });
        console.log()
      }
    }, (err) => {
      alert(err);
    });


  }



  
async saveImages(){
    console.log(this.imageResponse['0']);
    var localemail = localStorage['currentUser'];
    var data = {
      email: localemail,
      photo64: this.imageResponse['0']
      };
    console.log('Datos enviados para update user:', data);
    
    this.json.UpdateImg(data).subscribe((res: any ) =>{
      console.log('soy la respuesta',res);
      });
// //se llenara manualmente el arreglo imgResponse
// //mientras no tengo cordova en la pc... para desarollo
// this.imageResponse = [];
// this.imageResponse['0']='/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDACAWGBwYFCAcGhwkIiAmMFA0MCwsMGJGSjpQdGZ6eHJmcG6AkLicgIiuim5woNqirr7EztDOfJri8uDI8LjKzsb/2wBDASIkJDAqMF40NF7GhHCExsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsb/wAARCACWAMgDASIAAhEBAxEB/8QAGgAAAwEBAQEAAAAAAAAAAAAAAQIDAAQFBv/EADQQAAICAQMDAgMFBwUAAAAAAAABAhEDEiExBEFRE2EFInEyM5GhsRQVUlOBouEkQnKSwf/EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/8QAHBEBAQEBAAMBAQAAAAAAAAAAAAERAhIhMQNB/9oADAMBAAIRAxEAPwDx+pvVHxRELk5ctsARgwg5yUVyY9X4Z08Y4fVlu57brhWFcsIKEaS37+5WMaPR9OF7Qj+AV0MYv1cjqK30VyQceLE5/ZWy79joh08a+ZW3+RVtN7JRXZLhAbOV7v8AHScioqOyoOxKUmKm7ObStqhbFV2HdgHUhG0907QWtib2ZQXutxW9g2K2AVOS2TteHwLlwY86ehVJLhgboRy+eKctNvk3zazZHJPDPF9BYyUtu562WEcsdqs8zPicG5R87o6awCAwRlYbAVTcZew7yPxROSsaToDa234MJq7mCEMYxpBhHVNR8ns9LkdQxafZUeb00KqbTcntFI+g6LpfRgpz+9a334XgKpDHHFHXPn9Dny5Hklb7cIpmya3S+yiLjsce+t9R05gI2lyYVsFXZzaDSNFbcG7hcqAEq7itpbIllysRZPLC4u+CU1fcLntsKEKBr2GoDZRNs5+pg/llWy5OmSFkrjTNS4lQxZ9Eaa2LyrJj1L7S5+hzZI00q5OrpouPPg2zXBlholceAJ2dPUY9La8br6HK1p3XHc0yLEkrHW62NRURpoxXSYBCmPGmtUuF+YenxetlUW6XLfhHrYOijJpb+3siob4V0l11GTlbQj2Xud+eVR0p7sqkkqWyRzTlqyNox3cjXM9lcaQr8BlKhLODqNJIFgbBVgNYuXVWw6RmgOXS58qh1iSXF/UtQGNVKUVWwt0NNbEJqa3QFLFlJceRbVbi453J+DUSqVQjW5pyb4AoSkufwLiaWnJlcLtfR0ScpY1S/EfHK5an4Nxmt1S2i/6HHkhW64O/OrwP23OXZorLkdwdrjwMmmh5wae3BJxrdFQ5hIy8mA7vh+NRm3Ok3we508NMb8nkY79SKit20e3FVFLwaAyuscmjlOnN90zmOX6fW+SSEtjyFSuRydDRhfI1BukI5bhDXRnuCO4W1HdsSaMo3yCVJBbtauxN4/U+bhfqdPGRnSylHiyUpxXJbRCNrazjlGptrdPgmRo2SOtbEU3C6W5TJPTBy4QIwbjbW7LgWNyjd0Os0EuSOreUOGmCMHp+pcZU1OTe/wAr7D41vsSj2idMI0kEHL9zP6M447o6+oenp5/SjijwaiGZKap/UpYJboqISgnutjDPkxR7HRYv9Ut/sqz1Dzfh8tXVTfmL/VHpFQmb7pnM9kdc46oNHJKLUmmcv0b5JyMlQ1JKzUcmytAUbZStgxXJZNqWubPmePaEW779gY4zeWLbteSsY6pOLW1HB1OfL6koqWyezSo6RNdnUSWFJyd3wl3Iy6rLa0wS1du5zY23d7vtY7x5ZLV44sq47XFRirpy8nJ1bjWnu2J+0Zop657L2W5yzlknNqSbl2JIX0EscZvbKm/dlsGaSqGzruShi88l8eHe/wAy2kiM1L1JTfLZVRa80OsbbaaKKDk/ZE+lLjhvZZIaMKQyiaYcfxGWnFGP8T/Q5cUrVPkp1+TXmUVxDb+pCO3BcRegUaD1L3GoCM13MUkrMB6fw1V1Ev8Aj/6j0zy/h0kuoafLi0j0zSCQyxqWryXFktUWvJOpsWXEEFIRWpNS7D2efHQsmly0gqSaVb2Ty4tW63I6XjWpJtd0anpPq3qRxbNPUzm62LeJya44Ymul9ndj5JRyYVHJanE1q+OEwYtMbffsU0SkqbbSG6enj232Lae5naqHpRlGmhfRitklxVnQ4XwCSbW5ZKlsc3pwTfZp7m4dRX9Szg5PgeONJGpyzajHG+7KKCRRRNRrGSUJmmsOJzfbgtRw9ZP1JPGvsx/UDz5K3fkyQyXkJUCLadovFqStEaDCTiwqjRh9nujEFMU3jyRmuYuz2oyUoqS4atHiKmel0GXVicG7cOPoaR1GMYBMkNS9ycYvV8yLAaM3nbq6SkuAaLXHI7QNyWLrn9DG7uKsXJgUk01udNb33BW90TxXyc+PH6MK8obFkT2lsyzimiTxbrYZi7v1RoVxsdKjUbYIo0Z7Kx6NQE002GhtKsl1OePT47e8n9lEgn1OZYloT+eXBw18xNzlOeubbZXlJlRPLBJ3WzJUdckpwaOYBaMMABscqdPgwjMBUpiyyxzU48omarKPew5Y5sanHh/kOef0kpYoRrut15O6E4zVr8ACYIAAYIAAChgEAMEwAAEwAMaUlGLlJ0ly2cHUdf8A7cH/AGAv1PVQwR7Sl/CmeTOcss3KbtszuTbbtsFFBHxyq0+BDEF4umTzR31LgMXa90PtKDTA5jBktMmgAYxjAUCgJlcMNU0+y3ZR2xVJLwMpOLtNr6Cpme5BeHUx4mnflF01JWmmvY4dNmSrhlHcA459TlxQtVL6i/vLzi/u/wAAdxjg/eS/lf3f4N+81/J/u/wB3GOddW5RTjFK13ZKeXLLmW3sB2Skoq5NJHNm6xQg3ji5S9+CGlmoDjyZcuX7ybkIkVyY3CVduwlIAUChhb87AHsAK4MQBOmVi9yVBhKtgHzRtWuxA647o58kHCXsUIYKMBWUaZ09NGot+TGIOmjJGMA1GZjASzRvFL6HCzGKAzUYwHZj+7j9B0YwGoBjECZYqUH7bnE2YxQUwUYxAGq4A5NGMAewGYwFccrVjZVcL8bmMBzmMYo//9k=';
// //fin de la llenada manual





  }



}
