import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { map } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { NavController,LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-habilidades',
  templateUrl: './habilidades.page.html',
  styleUrls: ['./habilidades.page.scss'],
})
export class HabilidadesPage implements OnInit {

  habilidadesdelusuario: any;
  medicionesinputs: any;
  medicionesdrops: any;
  formGroup: FormGroup;
  formGroups: FormGroup;
  ranges = [];
  panges = [];
  paramsArray: FormArray;
  paramsArrays: FormArray;
  namesinput: any;
  namesdrop: any;
  algoo: any;
  alguu: string;
  hideMe: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private json: JsonService,
    private router: Router,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public alertController: AlertController,
    public loading: LoadingController

  )
  
  {
    localStorage.getItem('currentUser');
    this.habilidadesdelusuario= localStorage.getItem('habilidadesdelusuario');
  
    this.habilidadesdelusuario= JSON.parse(this.habilidadesdelusuario);

   }






   async  ngOnInit() {

    console.log('estas son las mediciones del usuario', this.habilidadesdelusuario);
    

    if( this.habilidadesdelusuario['nameonlyarray'].length!=0)

    {
      this.ranges = [{ranges: "Medicion Nombre"},];

  this.medicionesinputs = this.habilidadesdelusuario['a'];
  this.namesinput = this.habilidadesdelusuario['nameonlyarray'];



}   


if( this.habilidadesdelusuario['nameonlyarraydrops'].length!=0)

{

this.namesdrop = this.habilidadesdelusuario['nameonlyarraydrops'];
this.medicionesdrops = this.habilidadesdelusuario['b'];
this.panges = [{panges: "Medicion Nombre"},];


var temporal = new Array(); //se crea un array temporal
temporal = this.namesdrop;
console.log('asi quedo drops', temporal);
//se crean los inputs en la vista:    
var paramsArrays = new FormArray([]);
temporal.forEach(i=> {
var pangeArray = new FormArray([]);
paramsArrays.push(pangeArray);
//cantidad de clasicaciones de vista( 1, no utilizado en este TypeScrypt)          
this.panges.forEach(i=> {
  pangeArray.push(new FormControl(''))
});
  this.paramsArrays = paramsArrays;
});
//se rellenan los valores de formGoup para poder optener los valores segun se escriben:        
this.formGroups = new FormGroup({
  "valuess": this.paramsArrays,     
});
  
 

}  
    
        }
    
      
    
      async asd()
      {
    
        const alert = await this.alertController.create({
          header: 'LikeIU',
          subHeader: 'Informacion de registro:',
          message: 'Apenas empiecen los eventos, te notificaremos al correo.',
          buttons: ['OK']
        });
    
          // ///////////////////////se procedera a descomponer el array de lo tipiado/////////////////////////
          // localStorage.getItem('currentUser');//traigo usuario
          // //se pasa por el local storage para descomponer el array de lo tipiado.
          // console.log('valor no preparado arreglado', this.formGroup.value.values);
          // localStorage.setItem('lotipiado', this.formGroup.value.values);
          // this.algoo= localStorage.getItem('lotipiado');//se trae el array separado por comas por pasar x el cache
          // console.log('pasado popr el localstorage para descomponer array', this.algoo);
          // var algo = new Array(); 
          // algo= this.algoo.split(",");
          // console.log('valores en un solo array', algo );
          // ///////////////////////descompuesto en un solo array de 1 indice y 1 valor/////////////////////////
    
          // var data = {
          //     email: localStorage.currentUser,
          //     form_med_values1: algo,
          //     medicionesmostradas: this.medicionesinputs
          // }
    
          // console.log('estas 3 variables se han enviado para el inser en data', data)
    
          // this.json.insertarmedicioninput(data).subscribe((res: any ) =>{
          //     console.log('respuesta del inser', res);
          // });

          
          console.log('valor no preparado arreglado', this.formGroups.value.valuess);
          localStorage.setItem('lotipiado', this.formGroups.value.valuess);
          this.alguu= localStorage.getItem('lotipiado');//se trae el array separado por comas por pasar x el cache
          console.log('pasado popr el localstorage para descomponer array', this.alguu);
          var algu = new Array(); 
          algu= this.alguu.split(",");
          
          console.log('valores en un solo array', algu );
          var datadrop = {
              email: localStorage.currentUser,
              form_med_values1: algu,
              medicionesmostradas: this.medicionesdrops
          }
          this.json.insertarhabilidaddrop(datadrop).subscribe((res: any ) =>{
             console.log('respuesta del inser', res);
             this.router.navigate(['/tabs/home']);
    
             alert.present();
    
          });
    
          
        }

        hide() {

          if(this.hideMe==true){
        
          this.hideMe = false;
        }
        
          else{
        
            this.hideMe = true;
          };
        }
        



}
