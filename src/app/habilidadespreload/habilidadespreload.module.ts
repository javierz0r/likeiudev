import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HabilidadespreloadPage } from './habilidadespreload.page';

const routes: Routes = [
  {
    path: '',
    component: HabilidadespreloadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HabilidadespreloadPage]
})
export class HabilidadespreloadPageModule {}
