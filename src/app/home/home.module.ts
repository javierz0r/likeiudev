import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicRatingModule } from 'ionic4-rating';
import { PipesModule } from '../pipes/pipes.module';





import { HomePage } from './home.page';
// import { AnimatedLikeComponent } from '../components/animated-like/animated-like.component';



@NgModule({
  imports: [
    PipesModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    IonicRatingModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage
    ]
  
})
export class HomePageModule { }
