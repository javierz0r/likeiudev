import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagencompleteregistroPage } from './imagencompleteregistro.page';

describe('ImagencompleteregistroPage', () => {
  let component: ImagencompleteregistroPage;
  let fixture: ComponentFixture<ImagencompleteregistroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagencompleteregistroPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagencompleteregistroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
