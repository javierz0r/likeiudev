
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-imagencompleteregistro',
  templateUrl: './imagencompleteregistro.page.html',
  styleUrls: ['./imagencompleteregistro.page.scss'],
})
export class ImagencompleteregistroPage implements OnInit {

  hideMe: any;
  data: any;
  constructor( 
    public menu: MenuController,
    private activatedRoute: ActivatedRoute, 
    private router: Router) 
  
  
  { 

    this.activatedRoute.queryParams
    .subscribe((res: any)=>{
    this.data = res;//subscribing response
    console.log('recibi esto de la pagina anterior por parametros de navegacion', this.data);
  });
  
  
  }
  

  ngOnInit() {
  }

  hide() {

    if(this.hideMe==true){

    this.hideMe = false;
  }


    else{

      this.hideMe = true;
    };




  }
  

  backloginpage(){
    this.router.navigate(['/login'],);
  }

  guardarfotoiraocupaciones(){
    this.router.navigate(['/ocupacionescompleteregistro'],);
  }


  ionViewWillEnter() {
    this.menu.enable(false);
  }
  
  
  ionViewDidLeave() {
    // enable the root left menu because is login page.
    this.menu.enable(true);
  }

}
