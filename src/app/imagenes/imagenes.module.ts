import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';


import { ImagenesPage } from './imagenes.page';

const routes: Routes = [
  {
    path: '',
    component: ImagenesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    ImagePicker,
  ],
  declarations: [ImagenesPage]
})
export class ImagenesPageModule {}
