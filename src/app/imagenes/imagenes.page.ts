import { Component, OnInit } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { JsonService } from '../json.service';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { map } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { NavController,LoadingController, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-imagenes',
  templateUrl: './imagenes.page.html',
  styleUrls: ['./imagenes.page.scss'],
})
export class ImagenesPage implements OnInit {

  hideMe: boolean;
  tengolaimagen: any;
  imageResponse: any;
  options: any;

  medicionesinputs: any;
  medicionesdrops: any;
  medicionesdetalles: any;
  medicionesunidos: any;
  medis: any;
  todo = {};
  medicionesinput: any;
  players: any[];
  bids: string;
  formGroup: FormGroup;
  formGroups: FormGroup;
  ranges = [];
  panges = [];
  paramsArray: FormArray;
  paramsArrays: FormArray;
  params7 : any="";
  algoo: any;
  public connectedCspList:Array<string>=[];
  public connectedCspList2:any="";
  place: any;
  bidsb: any;
  params8: string;
  alguu: string;
  namesinput: any;
  namesdrop: any;
  caracteristicasdelusuario: any;
  tareasdelusuario: any;
  puedecontinuar:any;

  constructor
  (
    private imagePicker: ImagePicker,
    private formBuilder: FormBuilder,
    private json: JsonService,
    private router: Router,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public alertController: AlertController,
    public loading: LoadingController
  ) 
  
  { 
    localStorage.getItem('currentUser');
    this.tareasdelusuario= localStorage.getItem('tareasdelusuario');
  
    this.tareasdelusuario= JSON.parse(this.tareasdelusuario);

    this.hideMe = true;


  }

  async  ngOnInit() {

    

    if( this.tareasdelusuario['nameonlyarray'].length!=0)

    {



      console.log('estas son las mediciones del usuario', this.tareasdelusuario);
      
      
                this.medicionesinputs = this.tareasdelusuario['a'];
                this.namesinput = this.tareasdelusuario['nameonlyarray'];
                console.log('names array', this.namesinput);
      
      
          this.ranges = [
              {ranges: "Medicion Nombre"},
              ];
              this.panges = [
              {panges: "Medicion Nombre"},
              ];
      
      
     
      


          //   this.params7 = localStorage.getItem('medicionesenarray');
          //   console.log('asi me lo traje=', this.params7);
            var temp = new Array(); //se crea un array temporal
            //por cada valor separado por coma se almacena en cada indice del array
            temp = this.namesinput;
            console.log('asi quedo inputs', temp);
            //se crean los inputs en la vista:    
            var paramsArray = new FormArray([]);
            temp.forEach(i=> {
            var rangeArray = new FormArray([]);
            paramsArray.push(rangeArray);
            //cantidad de clasicaciones de vista( 1, no utilizado en este TypeScrypt)          
            this.ranges.forEach(i=> {
                rangeArray.push(new FormControl(''))
            });
                this.paramsArray = paramsArray;
            });
            //se rellenan los valores de formGoup para poder optener los valores segun se escriben:        
            this.formGroup = new FormGroup({
                "values": this.paramsArray,     
            });
      
      

      

    }
    else{

  this.puedecontinuar.nameonlyarray ='vacio';
    }

    
        }
    
      
    
      async asd()
      {
    
                const alert = await this.alertController.create({
                header: 'LikeIU',
                subHeader: 'Informacion de registro:',
                message: 'Complete formulario',
                buttons: ['OK']
                });


            if(this.puedecontinuar.input== 'si' || this.puedecontinuar.nameonlyarray== 'vacio'){
                this.router.navigate(['/home']);

            }
            else
            {
                alert.present();

            }
    
        }




        async getImages(event, image) {

          //me traje el evento clickeado con un click
          console.log('se clickeo esta imagen de todas las mostradas', image);     

var tomadadelclick = {
id_medicion: image.id_medicion,
}

          this.hideMe = false;
      
      
          this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            maximumImagesCount: 1,
        
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 200,
            height: 200,
            //height: 200,
        
            // quality of resized image, defaults to 100
            quality: 25,
        
            // output type, defaults to FILE_URIs.
            // available options are 
            // window.imagePicker.OutputType.FILE_URI (0) or 
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
          };
          this.imageResponse = [];
          this.imagePicker.getPictures(this.options).then(async (results) => {
            //se preparo imageResponse como un arreglo
            //se pueden almacenar varias segun la configuracion de arriba, 
            //pero como solo esta para una foto, el array 0 la contiene
            for (var i = 0; i < results.length; i++) {
      
              // this.imageResponse.push('data:image/jpeg;base64,' + results[i]);



      
              this.imageResponse.push(results[i]);
              // this.imageResponse['0']='asd';
              this.tengolaimagen=this.imageResponse['0'];
              console.log(this.imageResponse['0']);
              
              var localemail = localStorage['currentUser'];

              var data = {
                id_medicion: tomadadelclick.id_medicion,
                email: localemail,
                photo64: this.imageResponse['0']
                };
                image.temporal=this.imageResponse['0'];
                console.log(image.temporal);
                // img.temporal=this.imageResponse['0'];
                 const loading = await this.loadingController.create({
                  message: 'Cambiando Imagen',
                });   


                this.json.insertarimagecu(data).subscribe(async (res: any ) =>{
                    await loading.present();
                    console.log('soy la respuesta',res);
                    await loading.onDidDismiss();
                    });
                  
                  console.log('se tomara imagen  y se enviara esto:', data);
                }
                console.log('Datos enviados para update user:', data);

          }, (err) => {
            alert(err);
          });
      
      
        }



//  async continuarimagencomplete(){




//   var localemail = localStorage['currentUser'];
//   var data = {
//     id_medicion: '22',
//     email: localemail,
//     photo64: '2321312312312'
//     };

// console.log(data);

//     // img.temporal=this.imageResponse['0'];
//      const loading = await this.loadingController.create({
//       message: 'Cambiando Imagen',
//     });   
//     this.json.insertarimagecu(data).subscribe(async (res: any ) =>{
//         await loading.present();
//         console.log('soy la respuesta',res);
//         await loading.onDidDismiss();
//         });

  
//  }       







}
