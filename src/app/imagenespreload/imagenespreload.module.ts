import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ImagenespreloadPage } from './imagenespreload.page';

const routes: Routes = [
  {
    path: '',
    component: ImagenespreloadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ImagenespreloadPage]
})
export class ImagenespreloadPageModule {}
