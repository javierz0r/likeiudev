import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagenespreloadPage } from './imagenespreload.page';

describe('ImagenespreloadPage', () => {
  let component: ImagenespreloadPage;
  let fixture: ComponentFixture<ImagenespreloadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagenespreloadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagenespreloadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
