import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';
import { map } from "rxjs/operators";
import { Router } from "@angular/router";

@Component({
  selector: 'app-imagenespreload',
  templateUrl: './imagenespreload.page.html',
  styleUrls: ['./imagenespreload.page.scss'],
})
export class ImagenespreloadPage implements OnInit {
  medis: any;
  medicionesinputs: any;
  medicionesdrops: any;
  namesinput: any;
  namesdrop: any;
  constructor(

    private json: JsonService,
    private router: Router

  ) 
  
  {
    localStorage.getItem('currentUser');

   }

   ngOnInit() {

    //se procedera a filtrar las mediciones:::..
    var datafiltrado = {
      email: localStorage.currentUser,
    }
    
    console.log('Este usuario desea conocer sus imagenes correspondisntes segun sus profesiones:', datafiltrado)
    // this.json.filtrodemediciones(datafiltado).subscribe((res: any ) =>{
    //     console.log('respuesta del filtro', res);
    // });
    //filtradas.




        return new Promise(resolve => {
    
        this.json.filtrodeimages(datafiltrado)
        .pipe(map(res => res))
        .subscribe((res:any)  =>{



          
            this.medis = res;
            resolve(this.medis);

            if(res['nameonlyarray'].length!=0)
            {
              this.medicionesinputs = res['a'];
              resolve(this.medicionesinputs);
              this.namesinput = res['nameonlyarray'];
              resolve(this.namesinput);
              
            }
            
            // console.log('respuesta de todo', this.medis);
            // console.log('respuesta inputs', this.medicionesinputs);
            // console.log('respuesta drops', this.medicionesdrops);
            // console.log('respuestas de todas las medidas de drops', this.namesinput);
            // console.log('asi se guardo=', this.namesdrop);
            localStorage.setItem('tareasdelusuario', JSON.stringify(this.medis));
            console.log('se GUARDO Esto en el Cache, rediccionare...', this.medis);
            this.router.navigate(['/imagenes']);
    
        });
      
    });
    
    
      }


}
