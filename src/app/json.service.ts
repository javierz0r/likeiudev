import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http'//importo las respuestas nativas del servidor
import { map } from 'rxjs/operators'; //importo calculos reactivos de .map y .filter



@Injectable({
  providedIn: 'root'
})
export class JsonService {
  token: any;
  
  constructor( private http: HttpClient , private https: Http) {}


//   getJson()
//   {
//     var url = 'http://127.0.0.1:8000/api/qr';
//     return this.http.get(url);

//   }

//   postJson(data: any)
//   {

//   var url = 'http://127.0.0.1:8000/api/qr/refered';
//   return this.http.post(url,data,
//   {headers:new HttpHeaders({"Content-Type":'application/json'})});

// }

//   postRegister(data: any)
// {
//   var url = 'http://127.0.0.1:8000/api/register';
//   return this.http.post(url,data,
//   {headers:new HttpHeaders({"Content-Type":'application/json'})});

// }

//   postXp(data: any)
// {

//   var url = 'http://127.0.0.1:8000/api/xp/show';
//   return this.http.post(url,data,
//   {headers:new HttpHeaders({"Content-Type":'application/json'})});

// }

// postPoints(data: any)
// {

//   var url = 'http://127.0.0.1:8000/api/points/show';
//   return this.http.post(url,data,
//   {headers:new HttpHeaders({"Content-Type":'application/json'})});

// }


dameeliddeluser(data: any)
{
var url = 'https://api.likeiu.com/api/dameeliddeluser';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}




LoginGoogle()
{
  var url = 'https://api.likeiu.com/api/redirect';
  return this.http.get(url);
}


// LoginGoogle(data: any)
// {
// var url = 'https://api.likeiu.com/api/qr/refered';
// return this.http.post(url,data,
// {headers:new HttpHeaders({"Content-Type":'application/json'})});
// }

getJson()
{
  var url = 'https://api.likeiu.com/api/qr';
  return this.http.get(url);
}
postJson(data: any)
{
var url = 'https://api.likeiu.com/api/qr/refered';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

deidanombre(referidor: any)
{
var url = 'https://api.likeiu.com/api/deidanombre';
return this.http.post(url,referidor,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}



verilevelJson(data: any)
{
var url = 'https://api.likeiu.com/api/qr/verilevel';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

postRegister(data: any)
{
var url = 'https://api.likeiu.com/api/register';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postXp(data: any)
{
var url = 'https://api.likeiu.com/api/xp/show';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postPointshow(data: any)
{
var url = 'https://api.likeiu.com/api/points/show';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postShowPoints(data: any)
{
var url = 'https://api.likeiu.com/api/points/show';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


postPruebasPoints(data: any)
{
var url = 'https://api.likeiu.com/api/points/show';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}



postPoints(data: any)
{
var url = 'https://api.likeiu.com/api/points';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postFunction(data: any)
{
var url = 'https://api.likeiu.com/api/function/inventory';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


emailConfirmation(correodata: any)
{

var url = 'https://api.likeiu.com/api/enviodecodigo';
return this.http.post(url,correodata,
{headers:new HttpHeaders({"Content-Type":'application/json'})});

}


GetSearch()
{
  var url = 'https://api.likeiu.com/api/search';
  return this.http.get(url);

}


getDataAllPromotions(){
  
  var url = 'http://api.likeiu.com/api/promotionshow';
  return this.http.get(url);
  
  
}

getDataAllOcupations(){
  
  var url = 'http://api.likeiu.com/api/ocupationshow';
  return this.http.get(url);
  
  
}


getCategory(){
  
  var url = 'http://api.likeiu.com/api/category/show';
  return this.http.get(url);
  
}

PostSearch(data: any)
{

var url = 'https://api.likeiu.com/api/search';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});

}
  ClickOnApromotion(data: any)
  {
  
  var url = 'http://api.likeiu.com/api/promotions';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

  PostPromotion(data: any)
  {
  
  var url = 'http://api.likeiu.com/api/promotion';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

  PostFavoritelike(data: any)
  {
  
  var url = 'https://api.likeiu.com/api/favorite/like';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

  PostFavoritePromotion(data: any)
  {
  
  var url = 'https://api.likeiu.com/api/favorite/promotion/add';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

  PostFavoriteLocal(data: any)
  {
  
  var url = 'https://api.likeiu.com/api/favorite/local/add';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

  reviewshow(data: any)
  {
  var url = 'https://api.likeiu.com/api/reviews/show';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  }

reviewinsert(data: any)
{
var url = 'https://api.likeiu.com/api/rating/promo';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


RatingLocal(data: any)
{
var url = 'https://api.likeiu.com/api/rating/local';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


RatingLocal2(dataespecial: any)
{
var url = 'https://api.likeiu.com/api/rating/local2';
return this.http.post(url,dataespecial,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

//google validators
ValidateGoogle(data: any)
{
var url = 'https://api.likeiu.com/api/google/validate';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
UpdateGoogle(data: any)
{
var url = 'https://api.likeiu.com/api/google/update';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}



//ocupations jsons:
ocupationinser(data: any)
{
var url = 'https://api.likeiu.com/api/insertocupation/promo';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
ocupationupdate(data: any)
{
var url = 'https://api.likeiu.com/api/insertocupation/local';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

ocupationshow(data: any)
{
var url = 'https://api.likeiu.com/api/insertocupation/show';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

Codeshow()
{
var url = 'https://api.likeiu.com/api/vercodigo';
return this.http.get(url);
}


thiscodetothisuser(code: any)
{
var url = 'https://api.likeiu.com/api/thiscodetothisuser';
return this.http.post(url,code,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  }

  enviarcorreo(request: any)
  {
  var url = 'https://api.likeiu.com/api/contactar';
  return this.http.post(url,request,
    {headers:new HttpHeaders({"Content-Type":'application/json'})});
    }


  
    Medicionestodas()
    {
    var url = 'https://api.likeiu.com/api/mediciones/todas';
    return this.http.get(url);
    }

    Medicionestodasdetalles()
    {
      var url = 'https://api.likeiu.com/api/mediciones/todasdetalles';
      return this.http.get(url);
    }


  insertarmedicioninput(data: any)
  {
  var url = 'https://api.likeiu.com/api/mediciones/agregarinput';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  }

  insertarmediciondrop(datadrop: any)
  {
  var url = 'https://api.likeiu.com/api/mediciones/agregardrop';
  return this.http.post(url,datadrop,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  }



//validadores fin

UpdateImg(data: any)
{
var url = 'https://api.likeiu.com/api/user/updateimg';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

filtrodemediciones(data: any)
{
var url = 'https://api.likeiu.com/api/filtrodemediciones';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


//habilidades
insertarhabilidadinput(data: any)
{
var url = 'https://api.likeiu.com/api/habilidades/agregarinput';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

insertarhabilidaddrop(datadrop: any)
{
var url = 'https://api.likeiu.com/api/habilidades/agregardrop';
return this.http.post(url,datadrop,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

filtrodehabilidades(data: any)
{
var url = 'https://api.likeiu.com/api/filtrodehabilidades';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
  


//caracteristicas
insertarcaracteristicainput(data: any)
{
var url = 'https://api.likeiu.com/api/caracteristicas/agregarinput';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

insertarcaracteristicadrop(datadrop: any)
{
var url = 'https://api.likeiu.com/api/caracteristicas/agregardrop';
return this.http.post(url,datadrop,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

filtrodecaracteristicas(data: any)
{
var url = 'https://api.likeiu.com/api/filtrodecaracteristicas';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


//tareas
insertartareainput(data: any)
{
var url = 'https://api.likeiu.com/api/tareas/agregarinput';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

insertartareadrop(datadrop: any)
{
var url = 'https://api.likeiu.com/api/tareas/agregardrop';
return this.http.post(url,datadrop,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

filtrodetareas(data: any)
{
var url = 'https://api.likeiu.com/api/filtrodetareas';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


//estadisticas
insertarestadisticainput(data: any)
{
var url = 'https://api.likeiu.com/api/estadisticas/agregarinput';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

insertarestadisticadrop(datadrop: any)
{
var url = 'https://api.likeiu.com/api/estadisticas/agregardrop';
return this.http.post(url,datadrop,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

filtrodeestadisticas(datafiltrado: any)
{
var url = 'https://api.likeiu.com/api/filtrodeestadisticas';
return this.http.post(url,datafiltrado,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

//imagenes en regitro
insertarimageinput(data: any)
{
var url = 'https://api.likeiu.com/api/images/agregarinput';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

insertarimagedrop(datadrop: any)
{
var url = 'https://api.likeiu.com/api/images/agregardrop';
return this.http.post(url,datadrop,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

filtrodeimages(datafiltrado: any)
{
var url = 'https://api.likeiu.com/api/filtrodeimages';
return this.http.post(url,datafiltrado,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

insertarimagecu(data: any)
{
var url = 'https://api.likeiu.com/api/images/agregarimagencu';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}







}
