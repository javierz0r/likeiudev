import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LaravelPassportService } from 'laravel-passport';
import { JsonService } from '../json.service';
import { Router } from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { LoadingController, AlertController, Platform } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { environment } from '../../environments/environment';
import { MenuController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  displayName: any;
  familyName: any;
  givenName: any;
  userId: any;
  imageUrl: any;
  public userLogged: any;
  private user: FormGroup;
  // FB_APP_ID: number = 1286923721480310;
  loading: any;
  email: any;
  isLoggedIn:boolean = false;
  usergoogle: any;
  appverification: any;
  emailparalogear: any;
  
  constructor(
    private googlePlus: GooglePlus,
    private fireAuth: AngularFireAuth,
    public menu: MenuController,
    private google: GooglePlus,
    private platform: Platform,
    private json: JsonService,
    private router: Router,
    private laravelPassportService: LaravelPassportService,
    public modalController: ModalController,
    private nativeStorage: NativeStorage,
    public loadingController: LoadingController,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private http: HttpClient,
  ) {

    this.user = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    // this.LocalStorageSessionGet();
  }

  async ngOnInit() {
         this.loading = await this.loadingController.create({
          message: 'Connecting ...'
      });
    
  }
  async presentLoading(loading) {
    await loading.present();
  }

  async login() {

// el nivel de verificacion de un usuario.
    const user = this.user.value;
    var email = {email: user.email};
    //quiero saber mi nivel de verificacion:::
    this.json.verilevelJson(email).subscribe((res: any ) =>{
     console.log('Res De VerificationLevel=', res);
     this.appverification = res['0']['appverification'];
     this.emailparalogear = res['0']['email'];
     });






    const primerloadingexitoso = await this.loadingController.create({
      message: 'consultando, porfavor espere...',
      spinner: 'circles',
      duration: 2000,
    });
    const loadingexitoso = await this.loadingController.create({
      message: 'Ingreso Exitoso, Porfavor espere...',
      spinner: 'crescent',
      duration: 2000,
    });
    const loadingerror = await this.loadingController.create({
      message: 'ERROR: Porfavor, Verifique su correo y su contraseña...',
      spinner: 'dots',
      duration: 1000,
    });
    localStorage.setItem('currentUser', JSON.stringify(user.email));
    localStorage.setItem('isLoggedIn', 'true');
    this.laravelPassportService.loginWithEmailAndPassword(user.email, user.password).subscribe(
      res => {
        console.log(res);
        console.log('seccion abierta durante',res.expires_in);
        if (res.expires_in > 1) {
           primerloadingexitoso.present();
           this.presentLoading(loadingexitoso);
           console.log('Mostrando Alerta de Acceso Exitoso');


//nivel de verificacion igual a cero, viene de la web.
if(this.appverification=='0')
{
  var data = {
    email: user.email,
  };
  this.router.navigate(['/confirmesucorreo'],{queryParams: data});
}


else if(this.appverification=='1')
{
  //Al usuario se le proporciono el codigo de verificacion.
  var data = {
    email: user.email,
  };
  this.router.navigate(['/confirmesucorreo'],{queryParams: data});
}
else if(this.appverification=='2')
{
  //este usuario ya verifico su correo anteriormente pero no ha escogido sus ocupaciones.
  this.router.navigate(['ocupacionescompleteregistro']);
}
else if(this.appverification=='3')
{
    //el usuario concluyo todo el proceso de registro.
  this.router.navigate(['/tabs/home']);

}





           loadingexitoso.dismiss();
        } else{
          console.log('fallo en recuperacion de la respuesta');

        };

      },
      err => {
        console.log(err);
        console.log(err.statusText);
        if(err.statusText=="Unauthorized"){
          return  loadingerror.present();
    //      this.presentLoading(loadingerror)
          console.log('se ha detectado una respuesta no autorizada, se mostrara una alerta con el mensaje');
          loadingerror.dismiss();

        } else{
          console.log('error en recueracion de datos');

        };


      },
      () => {
        console.log('complete');
      }
    );
  }

  async LoginGoogle() {




    this.googlePlus.login({})
    .then(res => {console.log(res);

//inicia la segunda consulta
                  this.http
  .get('https://www.googleapis.com/plus/v1/people/me?access_token='+res.accessToken)
  .subscribe((datagoogleapi: any) => {
  console.log("esto es lo que realmente tiene la api de google en su reconsulta:",datagoogleapi); 
  var data = {
    name: res.givenName,
    email: res.email,
    last_name: res.familyName,
    photoURL: datagoogleapi.image.url,
    phone: datagoogleapi.phone,
    // userId: res.userId,
    provider_id: res.userId,
    provider: 'Google',
    };

  console.log('Datos Traidos de google Para LikeIU-DB:', data);
  localStorage.setItem('currentUser', JSON.stringify(res.email));
  this.json.UpdateGoogle(data).subscribe((res: any ) =>{
    console.log(res);
    localStorage.setItem('isLoggedIn', 'true');

//nivel de verificacion con el email traido de google en LikeIU:::
    var email = {email: data.email};
    //quiero saber mi nivel de verificacion:::
    this.json.verilevelJson(email).subscribe((res: any ) =>{
     console.log('Res De VerificationLevel=', res);
     this.appverification = res['0']['appverification'];


     if(this.appverification=='0')
     {
       var data = {
         email: data.email,
       };
       this.router.navigate(['/confirmesucorreo'],{queryParams: data});
     }

     else if(this.appverification=='1' || this.appverification=='2')
     {
       this.router.navigate(['ocupacionescompleteregistro']);
     }
     else{
       this.router.navigate(['/tabs/home']);
     }
     });
//usando el nivel de verificacion obtenido anteriormente:
    });


  });

    }
    )
    .catch(err =>{
      console.error(err);
       alert('error:' + JSON.stringify(err));
       alert
  }
    );




    
}



ionViewWillEnter() {

  // ToDo: No Mide el parametro verification status:::..
  // var logged = localStorage['isLoggedIn'];
  // if (logged =='true') {
  //     this.router.navigate(['/tabs/home']);
  //   }

  this.menu.enable(false);
}


ionViewDidLeave() {
  // enable the root left menu because is login page.
  this.menu.enable(true);
}





}
