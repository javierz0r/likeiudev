import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicionesPage } from './mediciones.page';

describe('MedicionesPage', () => {
  let component: MedicionesPage;
  let fixture: ComponentFixture<MedicionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicionesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
