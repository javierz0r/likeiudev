import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { map } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { NavController,LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-mediciones',
  templateUrl: './mediciones.page.html',
  styleUrls: ['./mediciones.page.scss'],
})
export class MedicionesPage implements OnInit {
  medicionesinputs: any;
  medicionesdrops: any;
  medicionesdetalles: any;
  medicionesunidos: any;
  medis: any;
  todo = {};
  medicionesinput: any;
  players: any[];
  bids: string;
  formGroup: FormGroup;
  formGroups: FormGroup;
  ranges = [];
  panges = [];
  paramsArray: FormArray;
  paramsArrays: FormArray;
  params7 : any="";
  algoo: any;
  public connectedCspList:Array<string>=[];
  public connectedCspList2:any="";
  place: any;
  bidsb: any;
  params8: string;
  alguu: string;
  namesinput: any;
  namesdrop: any;
  medicionesdelusuario: any;



  constructor
  ( private formBuilder: FormBuilder,
    private json: JsonService,
    private router: Router,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public alertController: AlertController,
    public loading: LoadingController

  )
  

  { 
  localStorage.getItem('currentUser');
  this.medicionesdelusuario= localStorage.getItem('medicionesdelusuario');

  this.medicionesdelusuario= JSON.parse(this.medicionesdelusuario);


  }


async ionViewDidEnter() {


// //se procedera a filtrar las mediciones:::..
// var datafiltado = {
//     email: localStorage.currentUser,
// }

// console.log('Este usuario desea conocer sus mediciones correspondisntes segun sus profesiones:', datafiltado)
// // this.json.filtrodemediciones(datafiltado).subscribe((res: any ) =>{
// //     console.log('respuesta del filtro', res);
// // });
// //filtradas.
//       return new Promise(resolve => {

//       this.json.filtrodemediciones(datafiltado)
//       .pipe(map(res => res))
//       .subscribe((res:any)  =>{
//           this.medis = res;
//           this.medicionesinputs = res['a'];
//           resolve(this.medicionesinputs);
//           this.medicionesdrops = res['b'];
//           resolve(this.medicionesdrops);
//           this.namesinput = res['nameonlyarray'];
//           resolve(this.bids);
//           this.namesdrop = res['nameonlyarraydrops'];
//           resolve(this.bidsb);
//         //   console.log('respuesta inputs', this.medicionesinputs);
//         //   console.log('respuesta drops', this.medicionesdrops);
//         //   console.log('respuestas de todas las medidas de drops', this.namesinput);
//         //   console.log('asi se guardo=', this.namesdrop);


//       });
    
// });
 

    
  }


async  ngOnInit() {

console.log(this.medicionesdelusuario);


  console.log('estas son las mediciones del usuario', this.medicionesdelusuario);



  

  // console.log('names array', this.namesinput);

                  if( this.medicionesdelusuario['nameonlyarray'].length!=0)

                  {
                    this.medicionesinputs = this.medicionesdelusuario['a'];
                    this.namesinput = this.medicionesdelusuario['nameonlyarray'];
                          
                          this.ranges = [
                          {ranges: "Medicion Nombre"},
                          ];

                          
                          
                          console.log('probando fuera de la suscripcion', this.medicionesinputs);
                          
                          
                          
                          var temp = new Array(); //se crea un array temporal
                          //por cada valor separado por coma se almacena en cada indice del array
                          temp = this.namesinput;
                          console.log('asi quedo inputs', temp);
                          //se crean los inputs en la vista:    
                          var paramsArray = new FormArray([]);
                          temp.forEach(i=> {
                          var rangeArray = new FormArray([]);
                          paramsArray.push(rangeArray);
                          //cantidad de clasicaciones de vista( 1, no utilizado en este TypeScrypt)          
                          this.ranges.forEach(i=> {
                          rangeArray.push(new FormControl(''))
                          });
                          this.paramsArray = paramsArray;
                          });
                          //se rellenan los valores de formGoup para poder optener los valores segun se escriben:        
                          this.formGroup = new FormGroup({
                          "values": this.paramsArray,     
                          });

                  };




            if(this.medicionesdelusuario['nameonlyarraydrops'].length!=0 )

            
            {
              this.panges = [
                {panges: "Medicion Nombre"},
                ];


              this.medicionesdrops = this.medicionesdelusuario['b'];
              this.namesdrop = this.medicionesdelusuario['nameonlyarraydrops'];
                    var temporal = new Array(); //se crea un array temporal
                    temporal = this.namesdrop;
                    console.log('asi quedo drops', temporal);


                    //se crean los inputs en la vista:    
                    var paramsArrays = new FormArray([]);
                    temporal.forEach(i=> {
                    var pangeArray = new FormArray([]);
                    paramsArrays.push(pangeArray);
                    //cantidad de clasicaciones de vista( 1, no utilizado en este TypeScrypt)          
                    this.panges.forEach(i=> {
                      pangeArray.push(new FormControl(''))
                    });
                      this.paramsArrays = paramsArrays;
                    });
                    //se rellenan los valores de formGoup para poder optener los valores segun se escriben:        
                    this.formGroups = new FormGroup({
                      "valuess": this.paramsArrays,     
                    });


            };



    }

  

  async asd()
  {

    const alert = await this.alertController.create({
      header: 'LikeIU',
      subHeader: 'Informacion de registro:',
      message: 'Apenas empiecen los eventos, te notificaremos al correo.',
      buttons: ['OK']
    });

      ///////////////////////se procedera a descomponer el array de lo tipiado/////////////////////////
      localStorage.getItem('currentUser');//traigo usuario
      //se pasa por el local storage para descomponer el array de lo tipiado.
      console.log('valor no preparado arreglado', this.formGroup.value.values);
      localStorage.setItem('lotipiado', this.formGroup.value.values);
      this.algoo= localStorage.getItem('lotipiado');//se trae el array separado por comas por pasar x el cache
      console.log('pasado popr el localstorage para descomponer array', this.algoo);
      var algo = new Array(); 
      algo= this.algoo.split(",");
      console.log('valores en un solo array', algo );
      ///////////////////////descompuesto en un solo array de 1 indice y 1 valor/////////////////////////

      var data = {
          email: localStorage.currentUser,
          form_med_values1: algo,
          medicionesmostradas: this.medicionesinputs
      }

      console.log('estas 3 variables se han enviado para el inser en data', data)

      this.json.insertarmedicioninput(data).subscribe((res: any ) =>{
          console.log('respuesta del inser', res);
      });
      console.log('valor no preparado arreglado', this.formGroups.value.valuess);
      localStorage.setItem('lotipiado', this.formGroups.value.valuess);
      this.alguu= localStorage.getItem('lotipiado');//se trae el array separado por comas por pasar x el cache
      console.log('pasado popr el localstorage para descomponer array', this.alguu);
      var algu = new Array(); 
      algu= this.alguu.split(",");
      console.log('valores en un solo array', algu );
      var datadrop = {
          email: localStorage.currentUser,
          form_med_values1: algu,
          medicionesmostradas: this.medicionesdrops
      }
      this.json.insertarmediciondrop(datadrop).subscribe((res: any ) =>{
         console.log('respuesta del inser', res);
         this.router.navigate(['/imagenespreload']);


      });

      
    }
    


}
