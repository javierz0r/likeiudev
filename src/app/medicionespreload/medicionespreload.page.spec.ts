import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicionespreloadPage } from './medicionespreload.page';

describe('MedicionespreloadPage', () => {
  let component: MedicionespreloadPage;
  let fixture: ComponentFixture<MedicionespreloadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicionespreloadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicionespreloadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
