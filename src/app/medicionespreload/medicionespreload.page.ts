import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';
import { map } from "rxjs/operators";
import { Router } from "@angular/router";

@Component({
  selector: 'app-medicionespreload',
  templateUrl: './medicionespreload.page.html',
  styleUrls: ['./medicionespreload.page.scss'],
})
export class MedicionespreloadPage implements OnInit {
  medis: any;
  medicionesinputs: any;
  medicionesdrops: any;
  namesinput: any;
  namesdrop: any;

  constructor(    
    private json: JsonService,
    private router: Router

    ) {

    localStorage.getItem('currentUser');


   }

  ngOnInit() {

//se procedera a filtrar las mediciones:::..
var datafiltado = {
  email: localStorage.currentUser,
}

console.log('Este usuario desea conocer sus mediciones correspondisntes segun sus profesiones:', datafiltado)
// this.json.filtrodemediciones(datafiltado).subscribe((res: any ) =>{
//     console.log('respuesta del filtro', res);
// });
//filtradas.
    return new Promise(resolve => {

      
      
      this.json.filtrodemediciones(datafiltado)
      .pipe(map(res => res))
      .subscribe((res:any)  =>{
        this.medis = res;
        resolve(this.medis);
console.log('respuesta nameonliarray:',res['nameonlyarray'].length, 'respuesta nameonlydrop',res['nameonlyarraydrops'].length); 

        if(res['nameonlyarray'].length!=0)
        {
          this.medicionesinputs = res['a'];
          resolve(this.medicionesinputs);
          this.namesinput = res['nameonlyarray'];
          resolve(this.namesinput);
          
        }
        


        if(res['nameonlyarraydrops'].length!=0)
        {

          this.medicionesdrops = res['b'];
          resolve(this.medicionesdrops);
          this.namesdrop = res['nameonlyarraydrops'];
          resolve(this.namesdrop);
        }




        localStorage.setItem('medicionesdelusuario', JSON.stringify(this.medis));
        console.log('se GUARDO Esto en el Cache, rediccionare...', this.medis);
        this.router.navigate(['/mediciones']);
    });
  
});


  }

}
