import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
@Component({
  selector: 'app-more-points',
  templateUrl: './more-points.page.html',
  styleUrls: ['./more-points.page.scss'],
})
export class MorePointsPage implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {
  }

   backfuctionbynavigation(){


      this._location.back();

}

}
