import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ocu2completeregistroPage } from './ocu2completeregistro.page';

describe('Ocu2completeregistroPage', () => {
  let component: Ocu2completeregistroPage;
  let fixture: ComponentFixture<Ocu2completeregistroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ocu2completeregistroPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ocu2completeregistroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
