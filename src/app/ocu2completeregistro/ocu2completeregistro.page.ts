import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { MenuController } from '@ionic/angular';
import { JsonService } from '../json.service';


@Component({
  selector: 'app-ocu2completeregistro',
  templateUrl: './ocu2completeregistro.page.html',
  styleUrls: ['./ocu2completeregistro.page.scss'],
})
export class Ocu2completeregistroPage implements OnInit {
  fuereferido: any;  texto1: any;  texto2:any;  texto3:any; texto4:any;  textomoneda: any;  texto5:any;  textovariable: string;
  iddelreferidor: any;
  referidorname: any;
  referidorlastname: any;
  apellido: any;
  constructor(
    private json: JsonService,
    public menu: MenuController,
    private router: Router
  ) 
  
  {
    localStorage.getItem('currentUser');

    var data = {
      email: localStorage['currentUser']
    };
    this.json.dameeliddeluser(data).subscribe((res: any ) =>{
    var fuereferido = res['0']['invited_by'];
    console.log("fue referido por",fuereferido);
    
    
    if(fuereferido!==null){
      //el usuario fue referido
      var referidor = {
        id: fuereferido,
      };
      //consultare el nombre del referidor para avisarle al referido que su referidor esta participando.
      this.json.deidanombre(referidor).subscribe((res: any ) =>{
        console.log("datos del referidor",res);
        this.referidorname= res['0']['name'];
        this.referidorlastname= res['0']['last_name'];
        this.texto1 = 'Gracias a ti';
      this.texto2 = 'tu amigo';
      // this.texto3 = 'roberto';
      this.texto3 = this.referidorname;
      this.apellido = this.referidorlastname;
      this.texto4 = 'esta participando por';
      this.textomoneda = '$';
      this.texto5 = '800';
      console.log('el usuario fue referido')


    });
    }
    else{
      this.texto1 = 'Gracias Por Registrarte';
      this.texto2 = 'Puedes ganar 800$';
      this.texto3 = 'si participas en el concurso';
      this.texto4 = 'solo debes generar y compartir';
      this.texto5 = 'tu codigo QR';
      console.log('el usuario NO fue referido')
    }
    
    

  });


   }
  ngOnInit() {    



  }
clicked()
{
  this.router.navigate(['http://likeiu.com']);
}

invitaramigos()

{
  // localStorage.setItem('currentUser', JSON.stringify(user.email));
  this.router.navigate(['/qrcode']);
}




iralhome()

{
  this.router.navigate(['/tabs/home']);
}

ionViewWillEnter() {
  this.menu.enable(false);
}

ionViewDidLeave() {
  // enable the root left menu because is login page.
  this.menu.enable(true);
}

}
