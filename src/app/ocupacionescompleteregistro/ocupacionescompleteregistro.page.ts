
import { Component, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { JsonService } from '../json.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { NavController,LoadingController, AlertController } from '@ionic/angular';
import { Router } from "@angular/router";
import { MenuController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-ocupacionescompleteregistro',
  templateUrl: './ocupacionescompleteregistro.page.html',
  styleUrls: ['./ocupacionescompleteregistro.page.scss'],
})
export class OcupacionescompleteregistroPage {


  @ViewChild('slidMore') slidMore:IonSlides;
  @ViewChild('slidNear') slidNear: IonSlides;
  @ViewChild('slidRecommen') slidRecommen: IonSlides;
  @ViewChild('slides') slider: IonSlides;
  @ViewChild('slideWithNav') slideWithNav: IonSlides;
 
  toggle01: boolean = true;
toggle02: boolean = false;
hideMe: any;



  // jsonData : any;
   sliderOne: any;
   segment = 0;
   
  //Configuration for each Slider
  slideOptsOne = {
    // spaceBetween:1,
    // slidesPerView:1.6,
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };

  sliderConfig ={
    spaceBetween:1,
    slidesPerView:1.8,
    initialSlide: 0,
    //slidesPerView: 1,
    autoplay:true
  };



  ocupations: any;
  filterItems: any;
  todosService: any;
  rating_starts: Event;
  public myForm: FormGroup;
  private playerCount: number = 1;

  constructor(
    private formBuilder: FormBuilder,
    public alertController: AlertController,
    public menu: MenuController,
    private googlePlus: GooglePlus,
    private nativeStorage: NativeStorage,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public loading: LoadingController,
    private json: JsonService,
    private router: Router
  ) 



  
  {
    localStorage.getItem('currentUser');
    console.log(localStorage);

    this.myForm = formBuilder.group({
      player1: ['', Validators.required]
    });
  }

  onSubmit(value: any) {
    console.log('Submittedddd: ', this.myForm);
  }

async addControl(){

    // this.myForm.addControl('player' + this.playerCount, new FormControl('', Validators.required));

if(this.playerCount<=9)
{
  this.playerCount++;
  this.myForm.addControl('player' + this.playerCount, new FormControl('', Validators.required));
}

else
{
      const maximocustomocupacion = await this.loadingController.create({
      message: 'El maximo de otros trabajos personalizados es 10',
      spinner: 'bubbles',
      duration: 900,
    });
    maximocustomocupacion.present();

  console.log('maximo permitido es 10')
}


  }

  removeControl(control){
    this.playerCount--;
    this.myForm.removeControl(control.key);
  }


  async ngOnInit()
   {
    const loading = await this.loadingController.create({
      message: 'Cargando, Porfavor espere...'
    });
    //agregado el ngOnInit
//whe need get all promotions without filter by the moments 
//because we dont erount filter parameters implemented
  this.json.getDataAllOcupations().subscribe((res: any) =>{
  this.ocupations = res;

  //mapeo el correo a todas las ocupaciones extraidas de la db
  this.ocupations
  .map(ocupation => {
    // localStorage.getItem('currentUser'),
    ocupation.email = localStorage.currentUser;
  });
//fin. 
  console.log('enable LikeIU Ocupations', this.ocupations);
});

  }






setHierarchyAsSelected(event: any) {

  this.ocupations
      .map(ocupation => {
        // localStorage.getItem('currentUser'),
        // ocupation.email = localStorage.currentUser,
        ocupation.isAssigned = event.target.value,
        console.log(this.ocupations);
      });

}


async cliccked()
{

  console.log('ARREGLO CON TRABAJOS CUSTOM: ', this.myForm.value);


if(this.playerCount>=1)
{
  console.log('Hay:',this.playerCount,'Trabajos customs, asi que se guardara un log con estos trabajos custom');  
  var dataespecial =
  {     
       email: localStorage.currentUser,
       custom1: this.myForm.value.player1,
       custom2: this.myForm.value.player2,
       custom3: this.myForm.value.player3,
       custom4: this.myForm.value.player4,
       custom5: this.myForm.value.player5,
       custom6: this.myForm.value.player6,
       custom7: this.myForm.value.player7,
       custom8: this.myForm.value.player8,
       custom9: this.myForm.value.player9,
       custom10: this.myForm.value.player10,
    };
    this.json.RatingLocal2(dataespecial).subscribe((res: any ) =>{
      console.log(res);
    });
 this.playerCount= 0;
}
  const alert = await this.alertController.create({
    header: 'LikeIU',
    subHeader: 'Informacion de registro:',
    message: 'Apenas empiecen los eventos, te notificaremos al correo.',
    buttons: ['OK']
  });

      return this.ocupations.filter((ocupations) => {
        if (ocupations.isAssigned== true) {
          console.log('selected ocuaption', ocupations);

          var rating =
          {
            // localStorage.setItem('currentUser', JSON.stringify(res.email));
             //ToDo: eliminar formato json del LocalStorage (EN TODA LA APP)
             //ToDo: ver porque si el usuario no existe en las tablas no se puede hacer ningun inser "IMPROVISADO".
            //El siguiente correo existe en la DB:
             // email:  JSON.stringify('javier@example.com'),
              //ToDo: Problema de email en el formato de envio encode.
               email: localStorage.currentUser,
              id_local: ocupations.id,
              rating_text: ocupations.isAssigned,
              rating_starts: ocupations.name
            };
            this.json.RatingLocal(rating).subscribe((res: any ) =>{


                //la suscripcion de llenado de profesiones se esta llenando
                //asi que procedere a verificarle la app a nivel 3.
                var code = {
                  // email: localStorage.currentUser,
                  // vericode: '9999',
                  appverification: '3',
                  email: localStorage.currentUser,
                  };
                this.json.thiscodetothisuser(code).subscribe((res: any ) =>{
                  console.log('respuesta del inser',res);
                });
              console.log(res);
            });
          return true;
        }

//toDo 0: Verificar la cantidad de insers que se pueden hacer para no clonar
//informacion.        
        var rating2 =
        {     
             email: localStorage.currentUser,
            id_local: ocupations.id,
            rating_text: '0',
            rating_starts: ocupations.name,
          };
          this.json.RatingLocal(rating2).subscribe((res: any ) =>{
            console.log(res);
                //la suscripcion de llenado de profesiones se esta llenando
                //asi que procedere a verificarle la app a nivel 3.
                var code = {
                  // email: localStorage.currentUser,
                  // vericode: '9999',
                  appverification: '3',
                  email: localStorage.currentUser,
                  };
                this.json.thiscodetothisuser(code).subscribe((res: any ) =>{
                  console.log('respuesta del inserta',res);
                  //si la respuesta de la actualzizacion de appverification es 0 quiere decir que se updateo y por ende, se redireccionara
                  //a la pagina de medicionesPreload para conocer las mediciones del usuario
                  if(res==0){
                
                    this.router.navigate(['medicionespreload']);
                    // alert.present();
                  
                  }
                });





          });




      });





}




hide() {

  if(this.hideMe==true){

  this.hideMe = false;
}

  else{

    this.hideMe = true;
  };
}




  private newMethod_1() {
    return this.newMethod();
  }

  private newMethod() {
    return '0';
  }

  ionViewWillEnter() {
    this.menu.enable(false);
  }
  
  
  ionViewDidLeave() {
    // enable the root left menu because is login page.
    this.menu.enable(true);
  }



}
