import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';

@Component({
  selector: 'app-points',
  templateUrl: './points.page.html',
  styleUrls: ['./points.page.scss'],
})
export class PointsPage implements OnInit {
  segment: string = "category";
  total_basic_points: number;
  historic: number;
  spent: number;
  spent_gold: number;
  total_gold_points: number;
  gold_historic: number;
  constructor(
    private json: JsonService
  ) {
    localStorage.getItem('currentUser');
   }

  ngOnInit()
   {
     var localemail = localStorage['currentUser'];
     var email = {email: localemail};
     this.json.postShowPoints(email).subscribe((res: any ) =>{
       console.log(res);

    //Basic Points
    this.total_basic_points = res.total_basic_points;
    this.spent = 0;
    this.historic = Number(this.total_basic_points) + Number(this.spent);
    if (this.total_basic_points == null)
    {
      this.total_basic_points = 0;
    }

     //Gold Points
     this.total_gold_points = res.total_gold_points;
     this.spent_gold = 0;
     this.gold_historic = Number(this.total_gold_points) + Number(this.spent_gold);
     if (this.total_gold_points == null)
     {
      this.total_gold_points = 0;
     }

     });
   }
  }
