import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from './../app.shared.module';
import { PromoDescriptionPage } from './promo-description.page';
import { AnimatedLikeComponent } from '../components/animated-like/animated-like.component';


const routes: Routes = [
  {
    path: '',
    component: PromoDescriptionPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PromoDescriptionPage,
    AnimatedLikeComponent
  ]
  
})
export class PromoDescriptionPageModule {}
