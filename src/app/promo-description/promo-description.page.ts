import { Component, OnInit, ViewChild  } from '@angular/core';
import {IonSlides} from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from "@angular/router";
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@ionic-native/file/ngx';
import { JsonService } from '../json.service';
import { Events } from '@ionic/angular';
import {Location} from '@angular/common';
@Component({
  selector: 'app-promo-description',
  templateUrl: './promo-description.page.html',
  styleUrls: ['./promo-description.page.scss'],
})
export class PromoDescriptionPage implements OnInit {
  localdata: any;
  clickedpromotion: any;
    /*shipping message */
    text= 'South American Comfort Food at Its Best!';
    url= 'https://www.elmonofresh.com/';
    /*shipping message */
    @ViewChild('rating') rating : any;
    @ViewChild('slideWithNav') slideWithNav: IonSlides;
     sliderOne: any;
     slideOptsOne = {
     initialSlide: 0,
     slidesPerView: 1,
     autoplay:true
    };
  id_function: any;
  point_granted: any;
  name: any;
  last_name: any;
  total_basic_points: any;
  total_gold_points: any;
  dataqr: any;
  promotions: any;
  declaracion: any;
  rating_starts: any;
  constructor(private _location2: Location,
    private activatedRoute: ActivatedRoute, 
    private router: Router, 
    private socialSharing: SocialSharing, 
    private file: File, 
    private json: JsonService,
    private alertCtrl: AlertController,
    public events: Events)

  { 
    localStorage.getItem('currentUser');

    this.sliderOne =
    {
      isBeginningSlide: true,
      isEndSlide: false,
      slidesItems: [
        {
          id: 1,
          image: '/src/assets/image/1.jpg'
        },
        {
          id: 2,
          image: '/src/assets/image/2.jpg'
        },
        {
          id: 3,
          image: '/src/assets/image/3.jpg'
        }
         
      ]
    };


  }

  logRatingChange(rating){
    console.log("changed rating local in alone view: ",rating);
    // do your stuff
}


  ngOnInit() {

    var localemail = localStorage['currentUser'];
    var email = {email: localemail};

      this.json.postJson(email).subscribe((res: any ) =>{
      this.name = res['0']['name'];
      this.last_name = res['0']['last_name'];
  });

    this.json.postShowPoints(email).subscribe((res: any ) =>{
      this.total_basic_points = res.total_basic_points;
      this.total_gold_points = res.total_gold_points;
      });
    // var id = this.id;
    var id_function = {id_function: 5};
    this.json.postFunction(id_function).subscribe((res: any ) =>{

    this.point_granted = res.point_granted
    this.id_function = res.id;

      console.log(this.id_function);
    });

      this.activatedRoute.queryParams.subscribe(res=>{
      this.clickedpromotion = res;//subscribing response
      console.log("Here, I've the objects",this.clickedpromotion);
        
      //in this view we have all clicked promotion data but by not exect bandwith why not have all promotion local data
      //in this order whi going to make another consult of the "local-promotion relation" only of the clicked promotion.
    });

  }

  async showPrompt() 
  {
    const alert = await this.alertCtrl.create({
      
      header: 'Rating',
      // message: "Please Enter yours comments",
      message: "<ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon>",
      inputs: [
        {
          name: 'title',
          placeholder: ''
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log(data);
            console.log('Saved clicked');
          }
        }
      ]
    });
    alert.present();
  }



  onGoToRestAsocPage()
  {
     this.router.navigate(['/tabs/restaurant-info'],{queryParams: this.clickedpromotion});
  }

  fuctionPoints()
  {
    var localemail = localStorage['currentUser'];
    var email = {email: localemail};
    var data =
    {
      id_function: this.id_function,
      email: email.email,
      point_granted: this.point_granted,
    };
    this.json.postPoints(data).subscribe((res: any ) =>{
    });
  }

  FavoritePromo()
  {
    var localemail = localStorage['currentUser'];
    var email = {email: localemail};
    var like = 1;
    this.clickedpromotion.id
    this.clickedpromotion.id_local
    // this.inconValue.toggleLikeState
    var data = 
    {
      email: email.email,
      like: like,
      id_promotion: this.clickedpromotion.id,
      id_local: this.clickedpromotion.id_local,
    };
    console.log(data)
    this.json.PostFavoritePromotion(data).subscribe((res: any ) =>{
    });
  }



  onGoToNextPage(event, promotion)
  {
    this.router.navigate(['/tabs/qr-promotion'],{queryParams: this.clickedpromotion});
  }

  slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }

  //Method called when slide is changed by drag or navigation
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
  }

  //Call methods to check if slide is first or last to enable disbale navigation  
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }

  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }


    //social networks in sharing
  async shareWhatsApp(){
    // Text + Image or URL works 
    this.socialSharing.shareViaWhatsApp(this.text, null, this.url).then(() =>{
      //success
    }).catch((e) => {
      //Error
    })
    }

    async shareFacebook(){
      this.socialSharing.shareViaFacebook(this.text, null, this.url).then((res) => {
        // Success
      }).catch((e) => {
        // Error!
      });
    }

    shareInstagram(){
      this.socialSharing.shareViaInstagram(this.text, this.url).then((res) => {
        // Success
      }).catch((e) => {
        // Error!
      });
    }

   


//     gotoreviewpage()
//     {
// this.rating['promotionimg']= this.clickedpromotion.photo,
// this.rating['promotionname']= this.clickedpromotion.name,
// this.rating['promotiondescription']= this.clickedpromotion.description,

//       this.router.navigate(['/reviewdetails'],{queryParams: this.rating});
//     }
    gotoreviewpage(event)
    {
      console.log(event);
      this.rating_starts = event;
      var localemail = localStorage['currentUser'];
      var email = {email: localemail};
      var rating =
      {
        email: email.email,
        id_promotion: this.clickedpromotion.id,
        id_local: this.clickedpromotion.id_local,
        rating_img: this.clickedpromotion.photo,
        rating_name: this.clickedpromotion.name,
        rating_description: this.clickedpromotion.description,
        rating_starts: this.rating_starts,
      };

      this.router.navigate(['/reviewdetails'],{queryParams: rating});
    }


 //   gotoreviewpage() {
      // this.navCtrl.push (ReviewdetailsPage, {rating: this.rating, promotion: this.promotions});
//    }
backdashboard(){


  this.router.navigate(['/tabs/home'],);

}


}

