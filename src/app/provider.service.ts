import { Injectable } from '@angular/core';
import { JsonService } from '../../src/app/json.service';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  constructor(
    private json: JsonService,
  )
   {

   }
}
