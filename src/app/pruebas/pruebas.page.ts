import { Component, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { JsonService } from '../json.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { NavController,LoadingController } from '@ionic/angular';
import { Router } from "@angular/router";
import { MenuController } from '@ionic/angular';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-pruebas',
  templateUrl: './pruebas.page.html',
  styleUrls: ['./pruebas.page.scss'],
})
export class PruebasPage {

  @ViewChild('slidMore') slidMore:IonSlides;
  @ViewChild('slidNear') slidNear: IonSlides;
  @ViewChild('slidRecommen') slidRecommen: IonSlides;
  @ViewChild('slides') slider: IonSlides;
  @ViewChild('slideWithNav') slideWithNav: IonSlides;
 
  lupa: any[] = [];
  promotion_name: any;
  promotion_description: any;
  promotion_local_name: any;
  promotion_name1: any;
  promotion_description1: any;
  promotion_local_name1: any;
  promotion_name2: any;
  promotion_description2: any;
  promotion_local_name2: any;
  // data: any;
  user: any;
  userReady: boolean = false;
  searchTerms: any="";
  food: any;
  search: any;
  
  // jsonData : any;
   sliderOne: any;
   segment = 0;
   
  //Configuration for each Slider
  slideOptsOne = {
    // spaceBetween:1,
    // slidesPerView:1.6,
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };

  sliderConfig ={
    spaceBetween:1,
    slidesPerView:1.8,
    initialSlide: 0,
    //slidesPerView: 1,
    autoplay:true
  };



  promotions: any;
  segundoenvio: any;
  localclickeddata: any;
  promo: any[] = [];
  local: any[] = [];

  constructor(
    public menu: MenuController,
    private googlePlus: GooglePlus,
    private nativeStorage: NativeStorage,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public loading: LoadingController,
    private json: JsonService,
    private router: Router
  ) 



  
  {
    localStorage.getItem('currentUser');
    console.log(localStorage);

  
//Item object for Nature
this.sliderOne =
{

  isBeginningSlide: true,
  isEndSlide: false,
  slidesItems: [
    {
      id: 111,
      image: '/src/assets/image/111.jpg'
    },
    {
      id: 222,
      image: '/src/assets/image/222.jpg'
    },
    {
      id: 333,
      image: '/src/assets/image/333.jpg'
    }
  ]
};


  }

  async ngOnInit()
   {
    const loading = await this.loadingController.create({
      message: 'Cargando, Porfavor espere...'
    });
    //agregado el ngOnInit
//whe need get all promotions without filter by the moments 
//because we dont erount filter parameters implemented
  this.json.getDataAllOcupations().subscribe((res: any) =>{
  this.promotions = res;
  console.log(this.promotions);
  
});

await loading.present();
     this.nativeStorage.getItem('google_user')
    .then(data => {
      this.user = {
        name: data.name,
        email: data.email,
        picture: data.picture,
      };
      this.userReady = true;
      loading.dismiss();
    }, error =>{
      console.log(error);
      loading.dismiss();
    });

    

  }
  
  doGoogleLogout(){
    this.googlePlus.logout()
    .then(res => {
      //user logged out so we will remove him from the NativeStorage
      this.nativeStorage.remove('google_user');
      this.router.navigate(["/login"]);
    }, err => {
      console.log(err);
    });
  }
  
  //evento de click a las mas visitadas:
  onGoToNextPage(event, promotion){
    console.log("ID To reconsult By Clic:",promotion.id);//consolelog of the clicked promotion related local.
          console.log("promotion clicked data before send",promotion);
          this.router.navigate(['/tabs/promo-description'],{queryParams: promotion});
  }
  ionViewDidLoad() {

    // this.PostSearch();
    // console.log(this.PostSearch);
 
  }

  SearchNextPage(event, promotion){
          this.router.navigate(['/tabs/promo-description'],{queryParams: promotion});
  }
  
  PostSearch(event)
  {
    const datas = event.target.value;
    if(datas === '')
    {
      return datas;
    }
    var data = {
      search: datas
    };
    
    // console.log(data);
    this.json.PostSearch(data).subscribe((res: any ) =>{
     this.promo = res;
     this.local = (res['0']['local']);

     console.log(res);
     console.log(this.local);
    //  console.log(res['0']['local']['0']['name']);

    //  this.promotion_name = res['0']['name'];
    //  this.promotion_description = res['0']['description'];
    //  this.promotion_local_name = res['0']['local']['0']['name'];
      
    //  this.searchTerms = [
    //  this.promotion_name,
    //  this.promotion_description,
    //  this.promotion_local_name,
    //   ];
    //  return this.lupa.filter();
    });
  }



  look()
{

}


   //Move to Next slide
   slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }

  //Method called when slide is changed by drag or navigation
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
  }

  //Call methods to check if slide is first or last to enable disbale navigation  
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }

  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }


//clic de los iconos en svg
vermilistadeinvitacionesview(){
  this.router.navigate(['/tabs/tab-listainvitaciones']);
}

masoportunidadesview(){
  this.router.navigate(['/tabs/tab-checatuseventos']);
}

vermicalendarioview(){
  this.router.navigate(['/tabs/tab-calendar']);
}

verdatosdecuentaview(){
  this.router.navigate(['/tabs/earn-points']);
}
//fin de  los clic de los iconos en svg




}
