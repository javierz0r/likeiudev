import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { JsonService } from '../json.service';
import { NgxQRCodeModule } from 'ngx-qrcode2';

import { IonicModule } from '@ionic/angular';

import { QrPromotionPage } from './qr-promotion.page';

const routes: Routes = [
  {
    path: '',
    component: QrPromotionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    NgxQRCodeModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  providers: [
  
    JsonService
 
  ],
  declarations: [QrPromotionPage]
})
export class QrPromotionPageModule {}
