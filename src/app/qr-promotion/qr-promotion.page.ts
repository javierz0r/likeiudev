import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-qr-promotion',
  templateUrl: './qr-promotion.page.html',
  styleUrls: ['./qr-promotion.page.scss'],
})
export class QrPromotionPage implements OnInit {
  QrToken: any;
  createdCode: any;
  clickedpromotion: any;

  constructor(
    private json: JsonService,
    private socialSharing: SocialSharing,
    private activatedRoute: ActivatedRoute, 
    private router: Router
  ) 
    {
    localStorage.getItem('currentUser');
    }

  ngOnInit()
   {
      var localemail = localStorage['currentUser'];
      var email = {email: localemail};

    
    this.activatedRoute.queryParams.subscribe(res=>{
    this.clickedpromotion = res.id;

    // console.log("Recibiendo datos de la otra Vista",this.clickedpromotion);
  });

  this.json.PostPromotion(this.clickedpromotion).subscribe((res: any ) =>{
    var qr_token = res['0']['promotion_qr'];
    this.QrToken = "http://api.likeiu.com/api/promotion"+qr_token;
    // console.log(qr_token);
  });

   }

   createCode()
  {
      this.createdCode = this.QrToken;
//     this.json.PostPromotion(this.clickedpromotion).subscribe((res: any) =>{
// });
  }


//social networks in sharing
async shareWhatsApp(){
  // Text + Image or URL works 
    this.socialSharing.shareViaWhatsApp(this.QrToken, null).then(() =>{
      //success
    }).catch((e) => {
      //Error
    })
  }

  async shareFacebook(){
    this.socialSharing.shareViaFacebook(this.QrToken, null).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  shareInstagram(){
    this.socialSharing.shareViaInstagram(this.QrToken, null).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }


}
