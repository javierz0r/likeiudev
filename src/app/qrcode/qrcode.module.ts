import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { JsonService } from '../json.service';

import { IonicModule } from '@ionic/angular';

import { QrcodePage } from './qrcode.page';

const routes: Routes = [
  {
    path: '',
    component: QrcodePage
  }
];

@NgModule({
  imports: [
   
    CommonModule,
    NgxQRCodeModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  providers: [
  
    JsonService
 
  ],
  declarations: [QrcodePage,
    ]
})
export class QrcodePageModule {}
