import { Component } from '@angular/core';
import { JsonService } from '../json.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
//import { File } from '@ionic-native/file/ngx';



@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.page.html',
  styleUrls: ['./qrcode.page.scss'],
})
export class QrcodePage {
  // data: any;
  // url: any;
  id: any;
  res: any
  name: any;
  last_name: any;
  email: any;
  QrToken :any;
  Token :any;
  tokel : any;
  createdCode: any;



  constructor(   
    private json: JsonService,
    private socialSharing: SocialSharing,
    //private file: File
  ) { 
    localStorage.getItem('currentUser');
    
  }
  insertreviewbutton() 
  {
    var localemail = localStorage['currentUser'];
    var email = {email: localemail};

    
  
    this.json.reviewinsert(email).subscribe((res: any ) =>{
    var qr_token = res['0']['qr_token'];
    this.QrToken = "http://api.likeiu.com/api/refered=$2y$10$7.e6344mJx9WU8d2fIliiO2hEDHyUc94X7vkuDvrtsGhWa9HpLbu6"+qr_token;
  });

  }

createCode()
{
  this.createdCode = this.QrToken;
  var data = {
    email: localStorage['currentUser']
  };
  this.json.dameeliddeluser(data).subscribe((res: any ) =>{
    console.log('respuesta completa', res);
    console.log('respuesta solo id', res['0']['id']);
    var qr_token = res['0']['id'];
    this.QrToken = "http://api.likeiu.com/api/refered=$2y$10$7.e6344mJx9WU8d2fIliiO2hEDHyUc94X7vkuDvrtsGhWa9HpLbu6"+qr_token;
    });
}


 //social networks in sharing
 async shareWhatsApp(){
  // Text + Image or URL works 
    this.socialSharing.shareViaWhatsApp(this.QrToken, null).then(() =>{
      //success
    }).catch((e) => {
      //Error
    })
  }

  async shareFacebook(){
    this.socialSharing.shareViaFacebook(this.QrToken, null).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  shareInstagram(){
    this.socialSharing.shareViaInstagram(this.QrToken, null).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

}
