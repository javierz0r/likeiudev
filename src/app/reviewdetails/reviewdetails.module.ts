import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from './../app.shared.module';
import { IonicModule } from '@ionic/angular';

import { ReviewdetailsPage } from './reviewdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ReviewdetailsPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReviewdetailsPage]
})
export class ReviewdetailsPageModule {}
