import { Component, OnInit, ViewChild } from '@angular/core';
import { Events } from '@ionic/angular';
import { ActivatedRoute, Router } from "@angular/router";
import {Http, Headers, RequestOptions}  from "@angular/http";
import { NavController, AlertController, NavParams } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { map } from 'rxjs/operators'; //importo calculos reactivos de .map y .filter
import { JsonService } from '../json.service';
import {Location} from '@angular/common';
@Component({
  selector: 'app-reviewdetails',
  templateUrl: './reviewdetails.page.html',
  styleUrls: ['./reviewdetails.page.scss'],
})
export class ReviewdetailsPage implements OnInit {
  QrToken: string;
  review_text: any;
  record: any;
  text_review: any;

  logRatingChange(rating){
    console.log("changed rating: ",rating);
    // do your stuff
  }

  @ViewChild("promo_or_local") promo_or_local;
  @ViewChild("username") username;
  @ViewChild("total_stars") total_stars;
  @ViewChild("textrevie") textrevie;
  @ViewChild("rating2") rating2;

  rating: any;
  ratingdeatras: any;
  promotions: any;

  constructor(
    private json: JsonService,
    private activatedRoute: ActivatedRoute, 
    public events: Events, 
    public alertCtrl: AlertController, 
    public loadingController: LoadingController, private router: Router, private _location: Location,
    private http: Http,
     ) {
    this.activatedRoute.queryParams.subscribe(res=>{
      this.rating = res;//subscribing response
      console.log("Raiting ",this.rating);
    });

  // let rating2 = this.rating;
  // console.log("rating de atras y mejorado alante:",this.rating2);
  // console.log("dato de promotion a realizarle el review:",this.promotions);
   }
   ngOnInit() {


  }
  async reviewpromo(){
    // localStorage.getItem('currentUser');
    // var email = localStorage['currentUser'];
    // console.log(localStorage.getItem('currentUser')); 

     this.review_text = this.text_review;
     console.log(this.review_text);

      var data = {
        email: this.rating.email,
        id_local: this.rating.id_local,
        id_promotions: this.rating.id_promotion,
        promotions_stars: this.rating.rating_starts,
        review_text: this.review_text,
      };
      console.log(data);    

      //este json se ejecutara segun algunos criterios de la navegacion
      const loading = await this.loadingController.create({ message: 'Sending, Please wait...'});
      await loading.present();
      this.json.reviewinsert(data).subscribe((res: any ) =>{ });
      this._location.back();
      loading.dismiss();    
}




}
