import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeBenefitsPage } from './see-benefits.page';

describe('SeeBenefitsPage', () => {
  let component: SeeBenefitsPage;
  let fixture: ComponentFixture<SeeBenefitsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeBenefitsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeBenefitsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
