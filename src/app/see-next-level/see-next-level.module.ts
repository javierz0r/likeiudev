import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SeeNextLevelPage } from './see-next-level.page';

const routes: Routes = [
  {
    path: '',
    component: SeeNextLevelPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SeeNextLevelPage]
})
export class SeeNextLevelPageModule {}
