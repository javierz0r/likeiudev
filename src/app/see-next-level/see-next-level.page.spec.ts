import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeNextLevelPage } from './see-next-level.page';

describe('SeeNextLevelPage', () => {
  let component: SeeNextLevelPage;
  let fixture: ComponentFixture<SeeNextLevelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeNextLevelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeNextLevelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
