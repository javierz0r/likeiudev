import { Injectable } from '@angular/core';
import { LaravelPassportModule } from 'laravel-passport';


@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private isUserLoggedIn;
  // public usserLogged:TabUserPage;

  constructor(
    
  ) { 
  	this.isUserLoggedIn = false;
  }

  // setUserLoggedIn(user:TabUserPage) {
  //   this.isUserLoggedIn = true;
  //   this.usserLogged = user;
  //   localStorage.setItem('currentUser', JSON.stringify(user));
  
  // }

  getUserLoggedIn() {
  	return JSON.parse(localStorage.getItem('currentUser'));
  }
}
