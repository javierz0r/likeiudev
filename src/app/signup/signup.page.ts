import { Component} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JsonService } from '../json.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage  {
    /**
@param formularioUsuario Rastrea el valor y el estado de validez de un grupo de instancias de FormControl.
   * Un FormGroup agrega los valores de cada FormControl hijo en un objeto, con cada nombre de control como la clave.
   * Calcula su estatus reduciendo los estatus de sus hijos. Por ejemplo, si uno de los controles de un grupo no es
   * válido, todo el grupo se convierte en no válido.
   * FormGroup es uno de los tres bloques de construcción fundamentales utilizados para definir formularios en Angular,
   * junto con FormControl y FormArray.
   */

  public changePasswordForm: FormGroup;   
  formularioUsuario:FormGroup;
data: any;
sigin: any;
email: any;
name: any;
last_name: any;
phone: any;
password: any;
password_confirmation: any;
  constructor(
    public menu: MenuController,
    private json: JsonService,
    private router: Router,
    public http: HttpClient,
    public loadingController: LoadingController,
        private alertCtrl: AlertController,
    private fb: FormBuilder
 
  )

{
    this.buildForm();
}
   async saveData() {

    const primerloadingexitoso = await this.loadingController.create({
      message: 'Cargando, porfavor espere...',
      spinner: 'circles',
      duration: 2000,
    });
    const loadingexitoso = await this.loadingController.create({
      message: 'Registro Exitoso, Porfavor espere...',
      spinner: 'crescent',
      duration: 2000,
    });
    const loadingerror = await this.loadingController.create({
      message: 'ERROR: El Correo Electronico ya existe...',
      spinner: 'dots',
      duration: 1300,
    });


    console.log(this.formularioUsuario.value);
    var data = {
      name: this.name,
      last_name: this.last_name,
      email: this.email,
      password: this.password,
      password_confirmation: this.password_confirmation,
      phone: this.phone,
      appverification: '1',
    };
    this.json.postRegister(data).subscribe(
      res => {
        console.log(res);
        console.log('seccion abierta durante',res);
        primerloadingexitoso.present();
        if (res=='Succes') {
           console.log('Mostrando Alerta de Acceso Exitoso',res);
           loadingexitoso.present();
           this.router.navigate(['/confirmesucorreo'],{queryParams: data});
           loadingexitoso.present();
        } else{
          console.log('fallo en recuperacion de la respuesta');

        };

      },
      err => {
        console.log('soy el error',err);
        console.log('desenpaquetando el error',err.error.errors.email[0]);
        if(err.error.errors.email[0]=="The email has already been taken."){
          loadingerror.present();

        } else{
          primerloadingexitoso.present();
          console.log(err.error.errors);
        };


      },
      () => {
        console.log('complete');
      }
    );


  }



  buildForm() {
    /** * @description Asignamos a la propiedad "formularioUsuario" los campos que se van a controlar de la vista  */
    this.formularioUsuario = this.fb.group({
      nombre:['',[Validators.required,Validators.maxLength(30)]],
      apellidoss:['',[Validators.required,Validators.maxLength(30)]],
      correo:['',[Validators.required,Validators.email]],
      tipo_contacto:['Telefono',[Validators.required]],
      newPassword: ['',[Validators.required,Validators.minLength(8),Validators.maxLength(30)]],
      confirmPassword:['',[Validators.required,Validators.minLength(8),Validators.maxLength(30)]],
      telefono:['',[Validators.required,Validators.minLength(8),Validators.maxLength(15)]],
    },{
            validator: this.MatchPassword // Inject the provider method
       });
  }
     private MatchPassword(AC: AbstractControl) {
        const newPassword = AC.get('newPassword').value // to get value in input tag
        const confirmPassword = AC.get('confirmPassword').value // to get value in input tag
         if(newPassword != confirmPassword) {
             console.log('Contraseñas no coinciden');
             AC.get('confirmPassword').setErrors( { MatchPassword: true } )
         } else {
             console.log('Coinciden las contraseñas')
             AC.get('confirmPassword').setErrors(null);
         }
     }
  backloginpage(){
    this.router.navigate(['/login'],);
  }




verificarcodigo(){
  
  this.router.navigate(['/imagencompleteregistro'],);

}


ionViewWillEnter() {
  this.menu.enable(false);
}


ionViewDidLeave() {
  // enable the root left menu because is login page.
  this.menu.enable(true);
}




}