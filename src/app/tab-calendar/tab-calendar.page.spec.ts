import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabCalendarPage } from './tab-calendar.page';

describe('TabCalendarPage', () => {
  let component: TabCalendarPage;
  let fixture: ComponentFixture<TabCalendarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabCalendarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabCalendarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
