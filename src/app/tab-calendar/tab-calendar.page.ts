import { Component, OnInit } from '@angular/core';
import { CalendarComponentOptions } from 'ion2-calendar';

@Component({
  selector: 'app-tab-calendar',
  templateUrl: './tab-calendar.page.html',
  styleUrls: ['./tab-calendar.page.scss'],
})
export class TabCalendarPage implements OnInit {

  dateMulti: string[];
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsMulti: CalendarComponentOptions = {
    pickMode: 'multi'
  };

  constructor() { }


  onChange($event) {
    console.log($event);
  }



  ngOnInit() {
  }

}
