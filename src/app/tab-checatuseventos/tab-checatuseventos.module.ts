import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabChecatuseventosPage } from './tab-checatuseventos.page';

const routes: Routes = [
  {
    path: '',
    component: TabChecatuseventosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabChecatuseventosPage]
})
export class TabChecatuseventosPageModule {}
