import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabChecatuseventosPage } from './tab-checatuseventos.page';

describe('TabChecatuseventosPage', () => {
  let component: TabChecatuseventosPage;
  let fixture: ComponentFixture<TabChecatuseventosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabChecatuseventosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabChecatuseventosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
