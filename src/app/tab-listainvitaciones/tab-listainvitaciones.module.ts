import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabListainvitacionesPage } from './tab-listainvitaciones.page';

const routes: Routes = [
  {
    path: '',
    component: TabListainvitacionesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabListainvitacionesPage]
})
export class TabListainvitacionesPageModule {}
