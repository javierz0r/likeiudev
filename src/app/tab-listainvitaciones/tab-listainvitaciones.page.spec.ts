import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabListainvitacionesPage } from './tab-listainvitaciones.page';

describe('TabListainvitacionesPage', () => {
  let component: TabListainvitacionesPage;
  let fixture: ComponentFixture<TabListainvitacionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabListainvitacionesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabListainvitacionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
