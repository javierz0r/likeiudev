import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabStarPage } from './tab-star.page';

describe('TabStarPage', () => {
  let component: TabStarPage;
  let fixture: ComponentFixture<TabStarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabStarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabStarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
