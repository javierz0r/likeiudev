import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabUserPage } from './tab-user.page';

describe('TabUserPage', () => {
  let component: TabUserPage;
  let fixture: ComponentFixture<TabUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabUserPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
