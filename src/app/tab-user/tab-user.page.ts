import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';
import { NavController, NavParams } from '@ionic/angular';
import { SeeBenefitsPage } from '../see-benefits/see-benefits.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-user',
  templateUrl: './tab-user.page.html',
  styleUrls: ['./tab-user.page.scss'],
})
export class TabUserPage implements OnInit {
  insignia: any;
  total_experience: any;
  necessary_experience_for_next_level: any;
  level: any;
  level_name: any;
  user_last_name: any;
  lastname: any;
  user: any;
  name: any;
  total_basic_points: any;
  provider: any;
  avatar: any;
  
  constructor
  ( private router: Router,
    private json: JsonService
    
  ) 
  {
    localStorage.getItem('currentUser');
    console.log('asd')
  }

  ionViewWillEnter(){

    var localemail = localStorage['currentUser'];
    var email = {email: localemail};
  
      // this.json.postJson(email).subscribe((res: any ) =>{
      // this.name = res['0']['name'];
      // this.last_name = res['0']['last_name'];
  
      this.json.postJson(email).subscribe((res: any ) =>{
        console.log('soy la respuesta para saber que llega al consultar user en postJson', res);
        this.name = res['0']['name'];
        this.lastname = res['0']['last_name'];
        this.avatar = res['0']['photoURL'];
  
  
  });
  this.json.postShowPoints(email).subscribe((respoints: any ) =>{
    console.log('soy la respuesta para saber que llega al consultar user en postShowPoints', respoints);
  this.total_basic_points = respoints.total_basic_points;
  });
  
  //      this.json.getJson().subscribe((res: any) =>{
  //      this.user = res.name;
  //      this.user_last_name = res.last_name;
  // });
  
  this.json.postXp(email).subscribe((exp :any)=>{
  console.log('soy la respuesta para saber que llega al consultar user en postXp', exp);
  this.insignia = exp['insignia'];
  this.level_name = exp['level_name'];
  this.level = exp['level'];
  this.necessary_experience_for_next_level = exp['necessary_experience_for_next_level'];
  this.total_experience = exp['total_experience'];
  
  
  
  console.log(exp);
  console.log(exp['level']);
  
  });
    
  }


  ngOnInit()
  {
    


  }

  goToNextPage () {
    this.router.navigate(['/tabs/see-benefits']);
  }

  goToNextLevel(){
    this.router.navigate(['/tabs/see-next-level']);
  }

  goToEarnPoints(){
    this.router.navigate(['/tabs/earn-points']);
  }




//clic de los iconos en svg
vermilistadeinvitacionesview(){
  this.router.navigate(['/tabs/tab-listainvitaciones']);
}

masoportunidadesview(){
  this.router.navigate(['/tabs/tab-checatuseventos']);
}

vermicalendarioview(){
  this.router.navigate(['/tabs/tab-calendar']);
}

verdatosdecuentaview(){
  this.router.navigate(['/tabs/earn-points']);
}
//fin de  los clic de los iconos en svg




  
}