import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
// import { GoogleMaps } from '@ionic-native/google-maps';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:[
      { path: 'home', loadChildren: '../home/home.module#HomePageModule' },
      { path: 'tab-user', loadChildren: '../tab-user/tab-user.module#TabUserPageModule' },
      { path: 'tab-search', loadChildren: '../tab-search/tab-search.module#TabSearchPageModule' },
      { path: 'tab-star', loadChildren: '../tab-star/tab-star.module#TabStarPageModule' },
      { path: 'tab-calendar', loadChildren: '../tab-calendar/tab-calendar.module#TabCalendarPageModule' },
      { path: 'tab-listainvitaciones', loadChildren: '../tab-listainvitaciones/tab-listainvitaciones.module#TabListainvitacionesPageModule' },
      { path: 'tab-checatuseventos', loadChildren: '../tab-checatuseventos/tab-checatuseventos.module#TabChecatuseventosPageModule' },
      { path: 'see-benefits', loadChildren: '../see-benefits/see-benefits.module#SeeBenefitsPageModule' },
      { path: 'qrcode', loadChildren: '../qrcode/qrcode.module#QrcodePageModule' },
      { path: 'see-next-level', loadChildren: '../see-next-level/see-next-level.module#SeeNextLevelPageModule' },
      { path: 'earn-points', loadChildren: '../earn-points/earn-points.module#EarnPointsPageModule' },
      { path: 'points', loadChildren: '../points/points.module#PointsPageModule' },
      { path: 'promo-description', loadChildren: '../promo-description/promo-description.module#PromoDescriptionPageModule' },
      { path: 'edit-profile', loadChildren: '../edit-profile/edit-profile.module#EditProfilePageModule' },
      { path: 'restaurant-info', loadChildren: '../restaurant-info/restaurant-info.module#RestaurantInfoPageModule' },
      { path: 'edit-profile', loadChildren: '../edit-profile/edit-profile.module#EditProfilePageModule' },
      { path: 'favorites', loadChildren: '../favorites/favorites.module#FavoritesPageModule' },
      { path: 'gallery', loadChildren: '../gallery/gallery.module#GalleryPageModule' },
      { path: 'more-points', loadChildren: '../more-points/more-points.module#MorePointsPageModule' },
      { path: 'qr-promotion', loadChildren: '../qr-promotion/qr-promotion.module#QrPromotionPageModule' },
      { path: 'eventcreate', loadChildren: '../eventcreate/eventcreate.module#EventcreatePageModule' },

   ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule, 
    IonicModule,
    RouterModule.forChild(routes)
  ],

  providers: [
    // GoogleMaps
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
