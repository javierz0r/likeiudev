import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json.service';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { map } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { NavController,LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.page.html',
  styleUrls: ['./tareas.page.scss'],
})
export class TareasPage implements OnInit {

  medicionesinputs: any;
  medicionesdrops: any;
  medicionesdetalles: any;
  medicionesunidos: any;
  medis: any;
  todo = {};
  medicionesinput: any;
  players: any[];
  bids: string;
  formGroup: FormGroup;
  formGroups: FormGroup;
  ranges = [];
  panges = [];
  paramsArray: FormArray;
  paramsArrays: FormArray;
  params7 : any="";
  algoo: any;
  public connectedCspList:Array<string>=[];
  public connectedCspList2:any="";
  place: any;
  bidsb: any;
  params8: string;
  alguu: string;
  namesinput: any;
  namesdrop: any;
  caracteristicasdelusuario: any;
  tareasdelusuario: any;


  constructor(
    private formBuilder: FormBuilder,
    private json: JsonService,
    private router: Router,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public alertController: AlertController,
    public loading: LoadingController



  ) 
  
  { 
    localStorage.getItem('currentUser');
    this.tareasdelusuario= localStorage.getItem('tareasdelusuario');
  
    this.tareasdelusuario= JSON.parse(this.tareasdelusuario);

  }



  async  ngOnInit() {


    if( this.tareasdelusuario['nameonlyarray'].length!=0)

    {



      console.log('estas son las mediciones del usuario', this.tareasdelusuario);
      
      
                this.medicionesinputs = this.tareasdelusuario['a'];
                this.namesinput = this.tareasdelusuario['nameonlyarray'];
                console.log('names array', this.namesinput);
      
      
          this.ranges = [
              {ranges: "Medicion Nombre"},
              ];
              this.panges = [
              {panges: "Medicion Nombre"},
              ];
      
      
     
      


          //   this.params7 = localStorage.getItem('medicionesenarray');
          //   console.log('asi me lo traje=', this.params7);
            var temp = new Array(); //se crea un array temporal
            //por cada valor separado por coma se almacena en cada indice del array
            temp = this.namesinput;
            console.log('asi quedo inputs', temp);
            //se crean los inputs en la vista:    
            var paramsArray = new FormArray([]);
            temp.forEach(i=> {
            var rangeArray = new FormArray([]);
            paramsArray.push(rangeArray);
            //cantidad de clasicaciones de vista( 1, no utilizado en este TypeScrypt)          
            this.ranges.forEach(i=> {
                rangeArray.push(new FormControl(''))
            });
                this.paramsArray = paramsArray;
            });
            //se rellenan los valores de formGoup para poder optener los valores segun se escriben:        
            this.formGroup = new FormGroup({
                "values": this.paramsArray,     
            });
      
      

      

    }

    
        }
    
      
    
      async asd()
      {
    
        const alert = await this.alertController.create({
          header: 'LikeIU',
          subHeader: 'Informacion de registro:',
          message: 'Apenas empiecen los eventos, te notificaremos al correo.',
          buttons: ['OK']
        });
    
          ///////////////////////se procedera a descomponer el array de lo tipiado/////////////////////////
          localStorage.getItem('currentUser');//traigo usuario
          //se pasa por el local storage para descomponer el array de lo tipiado.
          console.log('valor no preparado arreglado', this.formGroup.value.values);
          localStorage.setItem('lotipiado', this.formGroup.value.values);
          this.algoo= localStorage.getItem('lotipiado');//se trae el array separado por comas por pasar x el cache
          console.log('pasado popr el localstorage para descomponer array', this.algoo);
          var algo = new Array(); 
          algo= this.algoo.split(",");
          console.log('valores en un solo array', algo );

          ///////////////////////descompuesto en un solo array de 1 indice y 1 valor/////////////////////////
    
          var data = {
              email: localStorage.currentUser,
              form_med_values1: algo,
              medicionesmostradas: this.medicionesinputs
          }
    
          console.log('estas 3 variables se han enviado para el inser en data', data)
    
          this.json.insertartareainput(data).subscribe((res: any ) =>{
              console.log('respuesta del inser', res);
          });


          
        }




}
