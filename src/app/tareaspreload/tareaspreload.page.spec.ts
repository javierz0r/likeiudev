import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TareaspreloadPage } from './tareaspreload.page';

describe('TareaspreloadPage', () => {
  let component: TareaspreloadPage;
  let fixture: ComponentFixture<TareaspreloadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TareaspreloadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TareaspreloadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
